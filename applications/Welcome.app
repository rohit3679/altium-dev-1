<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
        <shouldOverrideOrgTheme>false</shouldOverrideOrgTheme>
    </brand>
    <description>Welcome to Financial Services Cloud!</description>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <label>Welcome</label>
    <navType>Standard</navType>
    <tabs>Getting_Started</tabs>
    <tabs>FundingRequest__c</tabs>
    <tabs>VigilanceService__c</tabs>
    <tabs>VigilanceOffering__c</tabs>
    <tabs>SkienceFinSln__Product_Type__c</tabs>
    <tabs>SkienceFinSln__Product_Selling_Agreement__c</tabs>
    <tabs>SkienceFinSln__Registration_Type_Selling_Agreement__c</tabs>
    <tabs>SkienceFinSln__Rep_Code_Detail__c</tabs>
    <tabs>SkienceFinSln__Registration_Type__c</tabs>
    <tabs>Underwriting__c</tabs>
    <uiType>Lightning</uiType>
    <utilityBar>Welcome_UtilityBar</utilityBar>
</CustomApplication>
