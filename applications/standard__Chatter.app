<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-Chatter</defaultLandingTab>
    <formFactors>Large</formFactors>
    <isNavAutoTempTabsDisabled>false</isNavAutoTempTabsDisabled>
    <isNavPersonalizationDisabled>false</isNavPersonalizationDisabled>
    <tabs>standard-Chatter</tabs>
    <tabs>standard-File</tabs>
    <tabs>FundingRequest__c</tabs>
    <tabs>VigilanceService__c</tabs>
    <tabs>VigilanceOffering__c</tabs>
    <tabs>SkienceFinSln__Product_Type__c</tabs>
    <tabs>SkienceFinSln__Product_Selling_Agreement__c</tabs>
    <tabs>SkienceFinSln__Registration_Type_Selling_Agreement__c</tabs>
    <tabs>SkienceFinSln__Rep_Code_Detail__c</tabs>
    <tabs>SkienceFinSln__Registration_Type__c</tabs>
    <tabs>Underwriting__c</tabs>
</CustomApplication>
