({
    doInit: function(component, event, helper) {
        // Fetch the account list from the Apex controller
        helper.getCaseList(component);
       
    },
    showAll:function(component, event, helper) {
        component.set("v.showAll",true);
    }
})