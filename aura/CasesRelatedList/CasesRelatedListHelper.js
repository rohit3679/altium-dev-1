({
    // Fetch the accounts from the Apex controller
    getCaseList: function(component) {
        var action = component.get('c.getCases');
        action.setParams({AccountID:component.get("v.recordId"),});
        var showAll = component.get("v.showAll");
        action.setCallback(this, function(actionResult) {
           	var firstSixRecs = actionResult.getReturnValue();
            if(!showAll) {
            	firstSixRecs = firstSixRecs.slice(0,6);
            }
            component.set('v.Case', firstSixRecs);
            var val =actionResult.getReturnValue();
             if(val.length>6){
                component.set('v.noofCases','6+'); 
            }
            else{
                component.set('v.noofCases',val.length);
            }
         });
        $A.enqueueAction(action);
    }
})