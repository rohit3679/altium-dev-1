({
    // Fetch the accounts from the Apex controller
    getAccountList: function(component) {
        var action = component.get('c.getAccounts');
        action.setParams({AccountID:component.get("v.recordId"),});
        var showAll = component.get("v.showAll");
        action.setCallback(this, function(actionResult) {
           	var firstSixRecs = actionResult.getReturnValue();
            if(!showAll) {
            	firstSixRecs = firstSixRecs.slice(0,6);
            }
            component.set('v.lineOfBusiness', firstSixRecs);
            var val =actionResult.getReturnValue();
            //alert(lineOfBusiness);
            if(val.length>6){
                component.set('v.nooflineOfBusiness','6+'); 
            }
            else{
                component.set('v.nooflineOfBusiness',val.length);
            }
         });
        $A.enqueueAction(action);
    }
})