/*****************************************************************************************************************
* Name         : AccountContactRelationTriggerHandler  
* @author      : Rohit
* @date        : 08/01/2019
* @group       : 
* @description : This is a hanler class for the trigger "AccountContactRelationTrigger".
* @copyright(c): 2019-2020, The Athene Group.
* Modification Log:
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rohit                      08/01/2019              Initial Creation
******************************************************************************************************************/
public class AccountContactRelationTriggerHandler {
    
    public static void handleAccountTeam(Map<Id,AccountContactRelation> acrMap){
        List<Id> childAccIdList = new List<Id>();
        List<Id> parentAccIdList = new List<Id>();
        Map<Id,List<AccountTeamMember>> atmMap = new Map<Id,List<AccountTeamMember>>();
        for(AccountContactRelation acr:acrMap.Values()){
            childAccIdList.add(acr.ContactAccountId__c);
            parentAccIdList.add(acr.AccountId);
        }
         List<AccountTeamMember> newTeamMemberList=[Select id, CaseAccessLevel,AccountId,AccountAccessLevel,ContactAccessLevel,
                                   OpportunityAccessLevel,TeamMemberRole,UserId from AccountTeamMember 
                                   where AccountId IN:childAccIdList ];
            if(newTeamMemberList.size()>0){
       
        for(AccountTeamMember atm: newTeamMemberList)
        {
            If(atmMap.containsKey(atm.AccountId)){
                List<AccountTeamMember> atmem = atmMap.get(atm.AccountId);
                atmem.add(atm);
                atmMap.put(atm.AccountId, atmem);
            } 
            else{
                atmMap.put(atm.AccountId, new List<AccountTeamMember>{atm});
            }
            
        }
           
        map<ID,ID> parentAndChildAccIdMap = new Map<ID, ID>();
        Map<Id,List<Id>> acrIds= new Map<Id,List<Id>>();
        for(AccountContactRelation acr: [Select id,Contact.Accountid,FinServ__Primary__c,AccountId From AccountContactRelation
                                         Where AccountId In:parentAccIdList])
        {
            if(acr.FinServ__Primary__c==True){
                parentAndChildAccIdMap.put(acr.AccountId, acr.Contact.Accountid);
            }
           
            If(acrIds.containsKey(acr.AccountId)){
                if(acr.FinServ__Primary__c!= True){
                    List<ID> acrmem = acrIds.get(acr.AccountId);
                    acrmem.add(acr.Contact.Accountid);
                    acrIds.put(acr.AccountId, acrmem);
                } 
            }
            else{
                List<Id> accids = new List<Id>();
                accids.add(acr.AccountId);
                if(acr.FinServ__Primary__c!= True)
                    accids.add(acr.Contact.Accountid);
                acrIds.put(acr.AccountId,accids);
            }
        }
        List<AccountTeamMember> atmList = new List<AccountTeamMember>();
        
        for(Id accid: acrIds.keySet()){
            for(Id aid:acrIds.get(accid) ){
                for(AccountTeamMember ateamm: atmMap.get(parentAndChildAccIdMap.get(accid))){
                    AccountTeamMember atm = new AccountTeamMember();
                    atm.AccountId = aid;
                    atm.CaseAccessLevel = ateamm.CaseAccessLevel;
                    atm.AccountAccessLevel = ateamm.AccountAccessLevel;
                    atm.ContactAccessLevel = ateamm.ContactAccessLevel;
                    atm.OpportunityAccessLevel = ateamm.OpportunityAccessLevel;
                    atm.TeamMemberRole = ateamm.TeamMemberRole;
                    atm.UserId = ateamm.UserId;
                    atmList.add(atm);
                }
            }
        }
        insert atmList;
    } 
         }
    public static void handleAccountTeamnewAccount(Map<Id,AccountContactRelation> acrMap){}
}