public class AccountTeamMembersUpdate {
    @InvocableMethod
    public static List <AccountTeamMember> getMembers(List<String> AccId) {
        Set<String> AccIdsSet = new Set<String>();
        Set<String> AccIdsSetprimary = new Set<String>();
        
        List<AccountContactRelation> accConListPrimary = [Select AccountId,Contact.Accountid From AccountContactRelation 
                                                          Where AccountId =:AccId and FinServ__Primary__c = true];
        
        for(AccountContactRelation accon : accConListPrimary){
            AccIdsSetprimary.add(accon.Contact.Accountid);
        }
        system.debug('Outside-- AccIdsSetprimary------>'+AccIdsSetprimary);  
        
        List<AccountContactRelation> accConList = [Select ID,AccountId,Contact.Accountid From AccountContactRelation
                                                   Where AccountId =:AccId];
        
        for(AccountContactRelation accon : accConList){
            AccIdsSet.add(accon.Contact.Accountid);
        }
        system.debug('AccIdsSet------>'+AccIdsSet);
        
        List<AccountTeamMember> accountteamlist = [Select Id, AccountId, Account.Name,USER.Name, TeamMemberRole From AccountTeamMember where AccountId In: AccIdsSetprimary];
        for(AccountTeamMember atm:accountteamlist){
            system.debug('Inside members--->'+accountteamlist);
        }
        system.debug('Team Members id--->'+accountteamlist);
        return null;
    }
}