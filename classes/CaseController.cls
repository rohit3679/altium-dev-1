public class CaseController {
    @AuraEnabled
    public static List <Case> getCases(Id AccountID) {
        Set<String> AccIdsSet = new Set<String>();
        List<AccountContactRelation> accConList = [Select Contact.Accountid From AccountContactRelation Where AccountId =:AccountID];
        for(AccountContactRelation accon : accConList){
            AccIdsSet.add(accon.Contact.Accountid);
            system.debug('inside-- conIdsSet------>'+AccIdsSet);
        }
        system.debug('AccIdsSet------>'+AccIdsSet);
        List<Case> lineList = [Select Id,Owner.Name, CaseNumber, Contact.AccountId,ContactId,Contact.Name, Subject,Status, Priority,AccountId,Account.Name from Case
                               Where AccountId IN: AccIdsSet];
                                   
        return lineList;
    }
}