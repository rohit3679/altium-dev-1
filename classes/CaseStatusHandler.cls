/*****************************************************************************************************************
* Name         : CaseStatusHandler  
* @author      : Rohit
* @date        : 07/12/2019
* @group       : 
* @description : This is a hanler class for the trigger "CaseStatus".
* @copyright(c): 2019-2020, The Athene Group.
* Modification Log:
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Rohit                      07/12/2019              Initial Creation
******************************************************************************************************************/
public class CaseStatusHandler {
    public static void InsertTwoNewCase(){
        List<Case> caseList = new List<Case>();
        Id csrec1 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Administrative Platform Set Up').getRecordTypeId();
        Id csrec2 = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Trading Platform Set Up').getRecordTypeId();
        Group g=[SELECT Id, Name, DeveloperName, Type FROM Group where Type='Queue'and DeveloperName='Portfolio_Operations'];
        
        for(Case cs : (list<case>)Trigger.New) {
            string recordtypename = Schema.SObjectType.Case.getRecordTypeInfosById().get(cs.recordtypeid).getname();
            if(cs.Status == 'Closed' && recordtypename=='New Account Opening' ) {
                if(cs.Id != null) { 
                    Case case1 = new Case();
                    case1.AccountId= cs.AccountId;
                    case1.RecordtypeId= csrec1;
                    case1.Status= 'New';
                    case1.OwnerId = g.Id;
                    case1.Type=cs.Type;
                    caseList.add(case1);
                    
                    Case case2 = new Case();
                    case2.AccountId= cs.AccountId;
                    case2.RecordtypeId = csrec2;
                    case2.Status= 'New';
                    case2.OwnerId = g.id;
                    case2.Type= cs.Type;
                    caseList.add(case2);
                }
            }
        }
        insert caseList;
    }
}