public class LineOfBusniessTriggerhandler {
    public void OnAfterInsert(List<Line_of_business__c> newLOB)
        {
			//updateHHfromRelatedLOB(newLOB);
			List<Line_of_business__c> updateLOBList = new List<Line_of_business__c>();
			for(Line_of_business__c lobtemp : newLOB)
                {
                    //run rollup logic only if the inserted lobs are active
					if(lobtemp.Status__c == 'Active')
						updateLOBList.add(lobtemp);
                }			
			updateHHfromRelatedLOB(updateLOBList);	
        }
    public void OnAfterUpdate( List<Line_of_business__c> newLOB, List<Line_of_business__c> oldLOB, Map<ID, Line_of_business__c> newLOBMap , Map<ID, Line_of_business__c> oldLOBMap )
        {
            List<Line_of_business__c> updateLOBList = new List<Line_of_business__c>();
			for(Line_of_business__c lobtemp : newLOB)
                {
					Line_of_business__c oldLOBtemp = oldLOBMap.get(lobtemp.Id);
                   //run rollup logic only if the LOB type or status changes
					if(oldLOBtemp.RecordTypeId != lobtemp.RecordTypeId || oldLOBtemp.Status__c != lobtemp.Status__c || oldLOBtemp.Amount__c != lobtemp.Amount__c)
						updateLOBList.add(lobtemp);
                }			
			updateHHfromRelatedLOB(updateLOBList);	
        }
    public void OnBeforeDelete( List<Line_of_business__c> newLOB, List<Line_of_business__c> oldLOB, Map<ID, Line_of_business__c> newLOBMap , Map<ID, Line_of_business__c> oldLOBMap )
        {
            //updateHHfromRelatedLOB(oldLOB);	
            List<Line_of_business__c> updateLOBList = new List<Line_of_business__c>();
			for(Line_of_business__c lobtemp : oldLOB)
                {
                    //run rollup logic only if the deleted lobs are active
					if(lobtemp.Status__c == 'Active')
						updateLOBList.add(lobtemp);
                }			
			updateHHfromRelatedLOB(updateLOBList);	
        }
	 public void updateHHfromRelatedLOB(List<Line_of_business__c> newLOB)
        {

			List<Id> lobClientIds = new List<Id>();         
            for(Line_of_business__c lobtemp : newLOB)
                {
                    lobClientIds.add(lobtemp.Client__c);
                }
            System.debug('Associated Acount '+lobClientIds);
            if(lobClientIds.size()>0)
            {
				List<Id> HHIds = new List<Id>();
				List<AccountContactRelation> acrLst = [SELECT AccountId,Contact.AccountId FROM AccountContactRelation where Contact.AccountId IN : lobClientIds AND Account.RecordType.DeveloperName = 'IndustriesHousehold'];		
				for(AccountContactRelation acr : acrLst )
                {	//Query all LOBs associated with the HHS
					HHIds.add(acr.AccountId);
				}
                System.debug('HHID '+HHIds);
				List<AccountContactRelation> HHPCLst = [SELECT AccountId,Contact.AccountId FROM AccountContactRelation where AccountId in : HHIds];
				List<Id> ClientIds = new List<Id>();
				//add hh & related PA ids
				Map<Id,List<id>> HHPAidsMap = new Map<Id,List<id>>();
				Map<ID,ID> accHH = new Map<ID,ID>();
				for(AccountContactRelation HHPC : HHPCLst )
				{
					ClientIds.add(HHPC.Contact.AccountId);
					//accHH.put(HHPC.Contact.AccountId,HHPC.AccountId);
					if(HHPAidsMap.containsKey(HHPC.AccountId))
					{
						List<Id> acctemplist = HHPAidsMap.get(HHPC.AccountId);
						acctemplist.add(HHPC.Contact.AccountId);
						HHPAidsMap.put(HHPC.AccountId,acctemplist);
					}
					else
					{
                        List<Id> acctemplist = new List<Id>();
						acctemplist.add(HHPC.Contact.AccountId);
						HHPAidsMap.put(HHPC.AccountId,acctemplist);
					}
					
				}
                System.debug('Linked clients to HH '+ClientIds);
                System.debug('Client and HH Map '+HHPAidsMap);

				Map<Id,List<Line_of_business__c>> HHLOBLISTMAP = new Map<Id,List<Line_of_business__c>>();
				/*List<Line_of_business__c> loblst = [Select id,amount__c,Client__c from Line_of_business__c where Client__c in :ClientIds];
				for(Line_of_business__c lob : loblst)
				{
					if(accHH.containsKey(lob.Client__c))
					{
						if(HHLOBLISTMAP.containsKey(accHH.get(lob.Client__c)))
							{
								List<Line_of_business__c> tempLob = HHLOBLISTMAP.get(accHH.get(lob.Client__c));
								tempLob.add(lob);
								HHLOBLISTMAP.put(accHH.get(lob.Client__c),tempLob);
							}
							else
							{
                                List<Line_of_business__c> tempLob = new List<Line_of_business__c>();
								tempLob.add(lob);
								HHLOBLISTMAP.put(accHH.get(lob.Client__c),tempLob);
							}
					}
				}*/
            List<Account> accLobList = [Select id,(Select id,amount__c,Client__c,RecordType.DeveloperName from Lines_of_business__r where Status__c = 'Active') from account where ID in :ClientIds]; 
			for(Account acc : accLobList)
            {
                List<Line_of_business__c> lob = acc.Lines_of_business__r;
                for(Id hhid: HHPAidsMap.KeySet())
                {
                    if((HHPAidsMap.get(hhid)).contains(acc.Id))
                    {
                        if(HHLOBLISTMAP.containsKey(hhid))
							{
								List<Line_of_business__c> tempLob = HHLOBLISTMAP.get(hhid);
								tempLob.addAll(lob);
								HHLOBLISTMAP.put(hhid,tempLob);
							}
							else
							{
                                List<Line_of_business__c> tempLob = new List<Line_of_business__c>();
								tempLob.addAll(lob);
								HHLOBLISTMAP.put(hhid,tempLob);
							}
                    }
                }
            }
           System.debug('HHLOBLISTMAP**********'+HHLOBLISTMAP);  
                for(Id tempId:HHLOBLISTMAP.keySet())
                {
                    for(Line_of_business__c test:HHLOBLISTMAP.get(tempId))
                    {
                    System.debug(tempId+'****************'+test);
                    }
                }
           List<Account> hhlst = [Select Financial_Planning_Sum__c,Vantage_Point_Sum__c,Vigilance_Sum__c,AUM_count_HH__c,Financial_Planning_count_HH__c,Fixed_Income_count_HH__c,Insurance_count_HH__c,Vantage_Point_count_HH__c,Vigilance_count_HH__c from account Where Id in : HHLOBLISTMAP.keySet()];
			for(Account a : hhlst)
                {
                     Decimal FinancialPlanning = 0;
                     Decimal FixedIncome = 0;
                     Decimal Insurance = 0;
                     Decimal VantagePoint = 0;
                     Decimal Vigilance = 0;
                     Decimal AUM_FinancialInvestment = 0;
                     Decimal FinancialPlanningSum = 0;
                     Decimal FixedIncomeSum = 0;
                     Decimal InsuranceSum = 0;
                     Decimal VantagePointSum = 0;
                     Decimal VigilanceSum = 0;
                     Decimal AUM_FinancialInvestmentSum = 0;
					if(HHLOBLISTMAP.containsKey(a.Id))
					{
						List<Line_of_business__c> temploblst = HHLOBLISTMAP.get(a.Id);
						for(Line_of_business__c l:temploblst)
						{
                            if(l.RecordType.DeveloperName == 'FinancialPlanning')
                            {
                            FinancialPlanning = FinancialPlanning + 1;
                                System.debug('*******l.amount__c '+l.amount__c);
                            FinancialPlanningSum = l.amount__c==null?FinancialPlanningSum:(FinancialPlanningSum + l.amount__c) ;
                                System.debug('*******FinancialPlanningSum '+FinancialPlanningSum);
                            }
                            else if(l.RecordType.DeveloperName == 'FixedIncome')
                            {
                            FixedIncome = FixedIncome + 1;
                            }
                            else if(l.RecordType.DeveloperName == 'Insurance')
                            {
                                Insurance = Insurance + 1;
                            }
                            else if(l.RecordType.DeveloperName == 'VantagePoint')
                            {
                                VantagePoint = VantagePoint + 1;
                                 System.debug('*******l.amount__c '+l.amount__c);
                                VantagePointSum = l.amount__c==null?VantagePointSum:(VantagePointSum + l.amount__c) ;
                                    System.debug('*******VantagePointSum '+VantagePointSum);
                            }
                            else if(l.RecordType.DeveloperName == 'Vigilance')
                            {
                                Vigilance = Vigilance + 1;
                                 System.debug('*******l.amount__c '+l.amount__c+'----------------'+VigilanceSum +'**************'+ l.amount__c);
                                VigilanceSum = l.amount__c==null?VigilanceSum:(VigilanceSum + l.amount__c) ;
                                    System.debug('*******VigilanceSum '+VigilanceSum);
                            }
                            else if(l.RecordType.DeveloperName == 'AUM_FinancialInvestment')
                            {
                                AUM_FinancialInvestment = AUM_FinancialInvestment + 1;
                            }
						}
					a.Financial_Planning_count_HH__c = FinancialPlanning;
					a.AUM_count_HH__c = AUM_FinancialInvestment;
					a.Fixed_Income_count_HH__c = FixedIncome;
					a.Insurance_count_HH__c = Insurance;
					a.Vantage_Point_count_HH__c = VantagePoint;
					a.Vigilance_count_HH__c = Vigilance;
                    a.Financial_Planning_Sum__c = FinancialPlanningSum ;
                    a.Vantage_Point_Sum__c = VantagePointSum ;
                    a.Vigilance_Sum__c = VigilanceSum ;

					}
                }
				update hhlst;
            }
        }
   
}