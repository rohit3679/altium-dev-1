public class LobController {
    @AuraEnabled
    public static List <Line_of_business__c> getAccounts(Id AccountID) {
        Set<String> conIdsSet = new Set<String>();
        List<AccountContactRelation> accConList = [Select ContactId From AccountContactRelation Where AccountId =:AccountID];
        for(AccountContactRelation accon : accConList){
            conIdsSet.add(accon.ContactId);
        }
        system.debug('conIdsSet------'+conIdsSet);
        List<Line_of_business__c> lineList = [Select Id,Name,Status__c,Opportunity__r.Name,Opportunity__c,RecordType.Name,FeeType__c,Amount__c,Client__c,Client__r.Name from Line_of_business__c
                                              Where Client__r.FinServ__PrimaryContact__c IN:conIdsSet];
        return lineList;
    }
}