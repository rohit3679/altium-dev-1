@isTest(SeeAllData=true)
public class RHX_TEST_FundingRequest {
	static testMethod void RHX_Testmethod() {
        List<sObject> sourceList = [SELECT Id 
			FROM FundingRequest__c LIMIT 1];
        if(sourceList.size() == 0) {
            sourceList.add(
                    new FundingRequest__c()
            );
        }
    	rh2.ParentUtil.UpsertRollupTestRecords( sourceList );
    }
}