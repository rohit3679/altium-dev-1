/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class AccountContactPreMatching {
    global AccountContactPreMatching() {

    }
    global virtual String PreMatchingLogic(String inputJson) {
        return null;
    }
    global static List<SkienceFinSln.AccountContactPreMatching.ContactAccountWrapper> deserializeFromStringToContactWrapper(String contactJson) {
        return null;
    }
    global static List<SkienceFinSln.AccountContactPreMatching.ResponseWrapper> deserializeFromStringToResponseWrapper(String inputJson) {
        return null;
    }
    global static SkienceFinSln.AccountContactPreMatching newInstance() {
        return null;
    }
global class ContactAccountWrapper {
    global String business_phone;
    global String client_status;
    global String dt_of_birth_or_organization;
    global String email_id;
    global String employer_name;
    global String fax;
    global String first_5_lastname;
    global String first_name;
    global String gender;
    global String home_phone;
    global String last_4_tax_id;
    global String last_or_entity_name;
    global String mailing_province;
    global String mailingcity;
    global String mailingcountry;
    global String mailingpostalcode;
    global String mailingstate;
    global String mailingStreet;
    global String marital_status;
    global String middle_name;
    global String mobile_phone;
    global String name_prefix;
    global String name_suffix;
    global String number_of_dependents;
    global String occupation;
    global String other_phone;
    global String other_province;
    global String othercity;
    global String othercountry;
    global String otherpostalcode;
    global String otherstate;
    global String otherstreet;
    global String owner_id;
    global String owner_type;
    global String sf_external_rec_id;
    global String tax_id;
    global String tax_id_type;
    global String w9_request_status_citizen;
    global String w9_request_status_tax_status;
    global String year_dob;
    global ContactAccountWrapper() {

    }
}
global class ResponseWrapper {
    global String error_message;
    global String is_success;
    global String recon_type;
    global String sf_account_row_id;
    global String sf_contact_row_id;
    global String sf_external_rec_id;
    global ResponseWrapper() {

    }
}
}
