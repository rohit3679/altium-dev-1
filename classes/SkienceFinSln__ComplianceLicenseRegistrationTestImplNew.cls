/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ComplianceLicenseRegistrationTestImplNew implements SkienceFinSln.IComplianceTestProcessor {
    global ComplianceLicenseRegistrationTestImplNew() {

    }
    global static SkienceFinSln.ComplianceTestController.ComplianceTestResult complianceTest(String financialAccountId, String complianceTestRunId, Id complianceTestConfigId) {
        return null;
    }
}
