/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/ComplianceTest/*')
global class ComplianceTestConfigController {
    global ComplianceTestConfigController() {

    }
    @HttpGet
    global static SkienceFinSln.ComplianceTestConfigController.ObjectToJsonTrade getTradeExceptionConfiguration() {
        return null;
    }
global class ObjectQuery {
    global ObjectQuery() {

    }
}
global class ObjectReturn {
    global ObjectReturn() {

    }
}
global class ObjectToJSONCompliance {
    global ObjectToJSONCompliance() {

    }
}
global class ObjectToJSONRemoteSuitability {
    global ObjectToJSONRemoteSuitability() {

    }
}
global class ObjectToJsonTrade {
    global ObjectToJsonTrade() {

    }
}
}
