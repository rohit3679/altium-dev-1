/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ComplianceTestController {
    global ComplianceTestController() {

    }
    @AuraEnabled
    global static SkienceFinSln.ComplianceTestController.ComplianceTestConfiguration getComplianceTestConfiguration(String financialAccountId) {
        return null;
    }
    @AuraEnabled
    global static Boolean runBulkRemoteTests(String financialAccountId, String complianceTestRunId) {
        return null;
    }
    @AuraEnabled
    global static SkienceFinSln.ComplianceTestController.ComplianceTestResult runComplianceTest(String financialAccountId, String complianceTestRunId, SkienceFinSln__Compliance_Test__mdt complianceTestRunConfig) {
        return null;
    }
    @AuraEnabled
    global static SkienceFinSln.ComplianceTestController.ComplianceTestResult runRemoteComplianceTest(String financialAccountId, String complianceTestRunId, SkienceFinSln__Compliance_Test__mdt complianceTestRunConfig) {
        return null;
    }
    @AuraEnabled
    global static String setComplianceTestResult(String financialAccountId, String complianceTestRunId) {
        return null;
    }
global class ComplianceTestConfiguration {
    global Id complianceTestRunId {
        get;
        set;
    }
    global List<SkienceFinSln__Compliance_Test__mdt> listComplianceTestRunConfig {
        get;
        set;
    }
    global ComplianceTestConfiguration() {

    }
}
global class ComplianceTestResult {
    global SkienceFinSln__Suitability_Test__c complianceTest {
        get;
        set;
    }
    @AuraEnabled
    global Id complianceTestConfigId;
    global List<SkienceFinSln__Suitability_Test_Configuration_Snapshot__c> listComplianceTestSnapshot {
        get;
        set;
    }
    global Boolean testApplies {
        get;
        set;
    }
    global ComplianceTestResult() {

    }
}
}
