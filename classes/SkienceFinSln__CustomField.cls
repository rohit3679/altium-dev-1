/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CustomField {
    global String controllingField {
        get;
        set;
    }
    global Object controllingFieldValue {
        get;
        set;
    }
    global String createRecordComponent {
        get;
        set;
    }
    global String createRecordType {
        get;
        set;
    }
    global String createRecordTypeId {
        get;
        set;
    }
    global String customFieldId {
        get;
        set;
    }
    global String cvControllingField {
        get;
        set;
    }
    global Object cvControllingFieldValue {
        get;
        set;
    }
    global Boolean cvControllingFieldVisible {
        get;
        set;
    }
    global Boolean disableOnSubmit {
        get;
        set;
    }
    global String extensionConfigId {
        get;
        set;
    }
    global String fieldAPIName {
        get;
        set;
    }
    global Object fieldDisplayValue {
        get;
        set;
    }
    global String fieldFriendlyName {
        get;
        set;
    }
    global String fieldName {
        get;
        set;
    }
    global Object fieldOriginalValue {
        get;
        set;
    }
    global String fieldPattern {
        get;
        set;
    }
    global String fieldPatternMessage {
        get;
        set;
    }
    global String fieldType {
        get;
        set;
    }
    global Object fieldValue {
        get;
        set;
    }
    global Boolean impactsRules {
        get;
        set;
    }
    global String inlineHelpText {
        get;
        set;
    }
    global Decimal length {
        get;
        set;
    }
    global List<String> listCVHideValue {
        get;
        set;
    }
    global List<String> listCVShowValue {
        get;
        set;
    }
    global List<SkienceFinSln.CustomField.PicklistDependency> listPicklistDependency {
        get;
        set;
    }
    global List<SkienceFinSln.CustomField.CustomPicklist> listPicklistValue {
        get;
        set;
    }
    global List<SkienceFinSln.CustomVisibilityConfig> listVisibilityConfig {
        get;
        set;
    }
    global String lookupRecordComponent {
        get;
        set;
    }
    global String lookupSelectionQuery {
        get;
        set;
    }
    global String objectName {
        get;
        set;
    }
    global Decimal precision {
        get;
        set;
    }
    global Boolean readOnly {
        get;
        set;
    }
    global String referenceTo {
        get;
        set;
    }
    global Boolean required {
        get;
        set;
    }
    global Boolean visible {
        get;
        set;
    }
    global Decimal visibleLines {
        get;
        set;
    }
    global CustomField() {

    }
global class CustomPicklist {
    @AuraEnabled
    global Boolean d;
    @AuraEnabled
    global String v;
    global CustomPicklist() {

    }
}
global class PicklistDependency {
    global String internal {
        get;
        set;
    }
    global List<String> listControlOption {
        get;
        set;
    }
    global String option {
        get;
        set;
    }
    global PicklistDependency() {

    }
}
}
