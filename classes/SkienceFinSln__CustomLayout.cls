/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CustomLayout {
    global String layoutId {
        get;
        set;
    }
    global List<SkienceFinSln.CustomSection> listCustomSection {
        get;
        set;
    }
    global String objectId {
        get;
        set;
    }
    global SkienceFinSln__Page_Manager__c pageManager {
        get;
        set;
    }
    global String recordTypeId {
        get;
        set;
    }
    global CustomLayout() {

    }
}
