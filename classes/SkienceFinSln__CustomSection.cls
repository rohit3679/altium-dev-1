/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CustomSection {
    global String customComponent {
        get;
        set;
    }
    global String cvControllingField {
        get;
        set;
    }
    global Object cvControllingFieldValue {
        get;
        set;
    }
    global Boolean cvControllingFieldVisible {
        get;
        set;
    }
    global List<SkienceFinSln.CustomFieldPair> listCustomFieldPair {
        get;
        set;
    }
    global List<String> listCVHideValue {
        get;
        set;
    }
    global List<String> listCVShowValue {
        get;
        set;
    }
    global List<SkienceFinSln.CustomVisibilityConfig> listVisibilityConfig {
        get;
        set;
    }
    global Boolean onePerLine {
        get;
        set;
    }
    global String sectionId {
        get;
        set;
    }
    global String sectionName {
        get;
        set;
    }
    global Boolean singleLine {
        get;
        set;
    }
    global Boolean visible {
        get;
        set;
    }
    global CustomSection() {

    }
}
