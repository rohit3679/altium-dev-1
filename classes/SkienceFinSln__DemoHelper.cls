/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class DemoHelper {
    global DemoHelper() {

    }
    global virtual String GetAccountPositions(String strFinancialAccountRowId, String strOptionsJson) {
        return null;
    }
    global virtual String GetAccountTransactions(String strFinancialAccountRowId, String strOptionsJson) {
        return null;
    }
    global virtual String RunOutBoundTransmission(String strTransactionJson, String strTradeExceptionJson, String strTradeReviewJson, String strOptionJson) {
        return null;
    }
    global virtual String generateNewCustodianDemoResponse(String strCustodian, String strAccountData, String strOptionsJson) {
        return null;
    }
    global virtual String generateUpdateCustodianDemoResponse(String strCustodian, String strAccountData, String strOptionsJson) {
        return null;
    }
    global virtual String getFAfldfromJSON(String JSONstr, String fldAPIName) {
        return null;
    }
    global virtual String getOFACResponse(String strAccountData) {
        return null;
    }
    global static SkienceFinSln.DemoHelper newInstance() {
        return null;
    }
    global virtual String transmitDocumentFolder() {
        return null;
    }
}
