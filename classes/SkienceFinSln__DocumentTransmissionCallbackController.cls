/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/DocumentTransmissionCallback/*')
global class DocumentTransmissionCallbackController {
    global DocumentTransmissionCallbackController() {

    }
    @HttpPost
    global static String postResponse() {
        return null;
    }
}
