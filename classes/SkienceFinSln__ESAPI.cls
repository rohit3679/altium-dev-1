/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ESAPI {
    global static SkienceFinSln.SFDCAccessController accessController() {
        return null;
    }
    global static SkienceFinSln.SFDCEncoder encoder() {
        return null;
    }
    global static SkienceFinSln.SFDCValidator validator() {
        return null;
    }
}
