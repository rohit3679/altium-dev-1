/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class EncryptedFieldMapper {
    global EncryptedFieldMapper() {

    }
    @InvocableMethod(label='Map Encrypted Fields' description='Maps encrypted fields between two objects')
    global static List<String> mapEncryptedFields(List<SkienceFinSln.EncryptedFieldMapper.MapEncryptedFieldsRequest> listMapEncryptedFieldsRequest) {
        return null;
    }
global class MapEncryptedFieldsRequest {
    @InvocableVariable( required=true)
    global String groupName;
    @InvocableVariable( required=true)
    global Id sourceId;
    @InvocableVariable( required=true)
    global Id targetId;
    global MapEncryptedFieldsRequest() {

    }
}
}
