/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class EnvestnetImpl {
    global EnvestnetImpl() {

    }
    global virtual String getClientPayload(Id hhId) {
        return null;
    }
    global virtual String getNewClientRequestBody(String strXMLPayload, SkienceFinSln.EnvestnetImpl.UserWrapper userResponse) {
        return null;
    }
    global virtual String getNewProposalRequestBody(SkienceFinSln__Proposal__c propRec, String clientHandle) {
        return null;
    }
    global virtual String getTranformXMLResponse(String strTranformXML, String strExceptionMsg) {
        return null;
    }
    global static SkienceFinSln.EnvestnetImpl newInstance() {
        return null;
    }
global class UserWrapper {
}
}
