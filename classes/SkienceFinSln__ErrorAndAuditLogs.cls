/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ErrorAndAuditLogs {
    global ErrorAndAuditLogs() {

    }
    global static void FailureEmail() {

    }
    global static void InsertLogs() {

    }
    global static void Log(String severity, String sMessage, String Process, String SubjRowId, String Action) {

    }
    global static void LogMessages(String severity, String sMessage, String Process, String SubjRowId, String Action) {

    }
global class myexception extends Exception {
}
}
