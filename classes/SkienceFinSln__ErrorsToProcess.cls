/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class ErrorsToProcess {
    global ErrorsToProcess() {

    }
    global void AppendError(Id recordId, String ErrMessage, String ErrDetail) {

    }
    global virtual void UpdateError(Boolean asynch) {

    }
}
