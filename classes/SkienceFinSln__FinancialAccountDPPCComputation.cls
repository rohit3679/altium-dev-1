/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class FinancialAccountDPPCComputation {
    global FinancialAccountDPPCComputation() {

    }
    global static List<SkienceFinSln.FinancialAccountDPPCComputation.FinancialWrapper> deserializeFromStringToFinAccWrapper(String financialAccountJson) {
        return null;
    }
    global virtual String dppcComputation(List<SkienceFinSln__DataProvider_ProductCompany__c> dppcList) {
        return null;
    }
    global virtual String findDPPCBasedOnDataProviderCode(String inputJson) {
        return null;
    }
    global static SkienceFinSln.FinancialAccountDPPCComputation newInstance() {
        return null;
    }
global class FinancialWrapper {
    global String account_id;
    global String account_name;
    global String account_number;
    global String account_registration_line;
    global String account_status;
    global String account_type;
    global String Data_Provider_Code;
    global String determined_product_company;
    global String dt_of_birth_or_organization;
    global String established_dt;
    global String first_name;
    global String last_or_entity_name;
    global String mailing_addr_line_1;
    global String mailing_addr_line_2;
    global String mailing_attention_line;
    global String mailing_city;
    global String mailing_country_code;
    global String mailing_extended_zip_code;
    global String mailing_state_code;
    global String mailing_zip_code;
    global String middle_name;
    global String owner_type;
    global String primary_rep;
    global String product;
    global String product_company;
    global String product_type;
    global String record_owner_id;
    global String sf_branch_code;
    global String sf_dp_product_company_row_id;
    global String sf_dp_row_id;
    global String sf_external_rec_id;
    global String sf_financial_account_row_id;
    global String sf_primary_contact_row_id;
    global String sf_rep_alias_row_id;
    global String tax_id;
    global String tax_id_type;
    global String vendor_rep_id;
    global FinancialWrapper() {

    }
}
global class ResponseWrapper {
    global String error_message;
    global Boolean is_success;
    global String sf_dp_product_company_row_id;
    global String sf_external_rec_id;
    global ResponseWrapper() {

    }
}
}
