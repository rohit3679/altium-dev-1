/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global interface IRestrictionProcessor {
    SkienceFinSln__Financial_Account_Restriction__c ignoreRestriction(String param0);
    String postRestrictions(String param0);
    SkienceFinSln__Financial_Account_Restriction__c retryRestriction(String param0);
}
