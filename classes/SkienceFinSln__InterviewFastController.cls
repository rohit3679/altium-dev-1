/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InterviewFastController {
    global InterviewFastController() {

    }
    global static void updateInterviewNavigation(String recordId, String redirectId, String redirectType, String finishId, String finishType, Decimal percentageComplete) {

    }
global class InterviewSaveError {
    global String errorMessage {
        get;
        set;
    }
    global String errorType {
        get;
        set;
    }
    global String fieldAPIName {
        get;
        set;
    }
    global InterviewSaveError() {

    }
}
global class InterviewSaveResult {
    global List<SkienceFinSln.InterviewFastController.InterviewSaveError> listSaveError {
        get;
        set;
    }
    global Id recordId {
        get;
        set;
    }
    global InterviewSaveResult() {

    }
}
}
