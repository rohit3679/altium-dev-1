/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InterviewUtils {
    global InterviewUtils() {

    }
    global static void createInitialPageManager(String recordId, String scDeveloperName, String objectName) {

    }
    global static void createPageManager(String recordId, String screenCollection, String objectName) {

    }
    global static String getAccountId(String contactId) {
        return null;
    }
    global static String getContactId(String accountId) {
        return null;
    }
    global static String getProposalHHId(String proposalId) {
        return null;
    }
    global static Boolean performDML(SObject sObj, String operation, List<SkienceFinSln.InterviewFastController.InterviewSaveResult> listInterviewSaveResult) {
        return null;
    }
}
