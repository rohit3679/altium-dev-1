/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global interface LaserAppMultiProcessorInterface {
    List<SkienceFinSln.LAAppNonStandardFld> processAll(SObject param0, SkienceFinSln__LaserAppConfiguration__c param1);
}
