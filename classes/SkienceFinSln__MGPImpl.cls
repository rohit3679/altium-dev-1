/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class MGPImpl {
    global MGPImpl() {

    }
    global virtual String getIdPURL() {
        return null;
    }
    global virtual String getMonteCarloMeter(String accountRecId) {
        return null;
    }
    global virtual String getMorningStarHealthMeterData(String houseHoldId) {
        return null;
    }
    global virtual String invokeService(Id AdvisorId, Id HouseholdId) {
        return null;
    }
    global static SkienceFinSln.MGPImpl newInstance() {
        return null;
    }
    global virtual SkienceFinSln.MGPImpl.MonteCarloResponse parseResponse(String strResponse) {
        return null;
    }
    global virtual String updateUserFinancialPlannerdetails(String accountRecId) {
        return null;
    }
global virtual class MonteCarloResponse {
    global String ErrorMessage {
        get;
        set;
    }
    global Boolean hasError {
        get;
        set;
    }
    global String HighlightHtmlColor {
        get;
        set;
    }
    global String Image {
        get;
        set;
    }
    global String ImageExtension {
        get;
        set;
    }
    global String IsLegacyImage {
        get;
        set;
    }
    global String ProbabilityOfSuccess {
        get;
        set;
    }
    global String RefreshPositionsResponse {
        get;
        set;
    }
    global String TargetRangeHigh {
        get;
        set;
    }
    global String TargetRangeLow {
        get;
        set;
    }
    global String TargetText {
        get;
        set;
    }
    global MonteCarloResponse() {

    }
}
}
