/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MGPIntegration {
    global MGPIntegration() {

    }
    webService static String IdPURL() {
        return null;
    }
    webService static String Skience2Mgp(Id HouseholdId, Id AdvisorId, Id UserId) {
        return null;
    }
}
