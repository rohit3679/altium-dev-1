/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class MorningStarImpl {
    global MorningStarImpl() {

    }
    global virtual Boolean checkMorningStarUserPrivileges() {
        return null;
    }
    global virtual String generateAndUpdatePayloadInfo(String accountRecId, String actionType) {
        return null;
    }
    global virtual String getApplicationType(String strActiontype) {
        return null;
    }
    global virtual String getIdPURL() {
        return null;
    }
    global virtual String getPayload(Id idHousehold, String actionType) {
        return null;
    }
    global virtual String invokeService(Id idHousehold, String actionType) {
        return null;
    }
    global static SkienceFinSln.MorningStarImpl newInstance() {
        return null;
    }
    global virtual String parseResponse(Id idHH, String strResponse) {
        return null;
    }
}
