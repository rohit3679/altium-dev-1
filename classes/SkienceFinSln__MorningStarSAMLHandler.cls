/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MorningStarSAMLHandler extends Auth.ConnectedAppPlugin {
    global MorningStarSAMLHandler() {

    }
    global override Boolean authorize(Id userId, Id connectedAppId, Boolean isAdminApproved) {
        return null;
    }
    global override Map<String,String> customAttributes(Id userId, Map<String,String> formulaDefinedAttributes) {
        return null;
    }
    global override dom.XmlNode modifySAMLResponse(Map<String,String> authSession, Id connectedAppId, dom.XmlNode samlResponse) {
        return null;
    }
    global override void refresh(Id userId, Id connectedAppId) {

    }
}
