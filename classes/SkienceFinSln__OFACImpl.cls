/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class OFACImpl {
    global SkienceFinSln__OFAC_CIP_Check_Run__c m_recRunHeader;
    global SkienceFinSln__Snapshot_Group__c m_recSnapshot;
    global List<SkienceFinSln__OFAC_CIP_Check__c> m_recsRunDetail;
    global OFACImpl() {

    }
    global virtual String invokeService(Id idFA) {
        return null;
    }
    global static SkienceFinSln.OFACImpl newInstance() {
        return null;
    }
    global virtual void parseResponse(Id idFA, String strResponse) {

    }
    global virtual void persistRunResult() {

    }
    global virtual String runTest(Id idFA) {
        return null;
    }
}
