/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ObjectCloneController {
    global ObjectCloneController() {

    }
    global static List<SkienceFinSln.ObjectCloneController.ObjectClone> cloneRecord(String objectId, String jsonType) {
        return null;
    }
    global static List<SkienceFinSln.ObjectCloneController.ObjectClone> cloneRecord(String objectId, String jsonType, Integer noOfTimes) {
        return null;
    }
    global static List<SkienceFinSln.ObjectCloneController.ObjectClone> cloneRecord(String objectId, String jsonType, String newParentId) {
        return null;
    }
global class ObjectClone {
    global ObjectClone() {

    }
}
}
