/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/ObjectToJSON/*')
global class ObjectToJSONController {
    global ObjectToJSONController() {

    }
    @HttpGet
    global static SkienceFinSln.ObjectToJSONController.ObjectToJSONWrapper getObjectToJSON() {
        return null;
    }
    global static SkienceFinSln.ObjectToJSONController.ObjectToJSONWrapper getObjectToJSON(String objectId) {
        return null;
    }
    global static SkienceFinSln.ObjectToJSONController.ObjectToJSONWrapper getObjectToJSON(String objectId, String jsonType) {
        return null;
    }
global virtual class ObjectQuery {
    global String additionalFilter;
    global String excludeFields;
    global String idFieldLHS;
    global String idFieldRHS;
    global String objectName;
    global List<SkienceFinSln.ObjectToJSONController.ObjectQuery> related;
    global List<SObject> result;
    global ObjectQuery() {

    }
}
global virtual class ObjectReturn {
    global String name;
    global SObject record;
    global List<SkienceFinSln.ObjectToJSONController.ObjectReturn> related;
    global ObjectReturn() {

    }
}
global virtual class ObjectToJSONWrapper {
    global SkienceFinSln.ObjectToJSONController.ObjectQuery objectQuery;
    global List<SkienceFinSln.ObjectToJSONController.ObjectReturn> objectReturn;
    global ObjectToJSONWrapper() {

    }
}
}
