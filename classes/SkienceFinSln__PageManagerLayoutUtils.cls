/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PageManagerLayoutUtils {
    global PageManagerLayoutUtils() {

    }
    global static void deleteAllLayout() {

    }
    global static void updateLayoutInfo() {

    }
    global static void updateTabSummaryInfo(String s1, String s2, String s3, String s4, Integer i) {

    }
}
