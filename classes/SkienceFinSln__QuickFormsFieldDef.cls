/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class QuickFormsFieldDef {
    global String FieldCalcOverride {
        get;
        set;
    }
    global String FieldFormat {
        get;
        set;
    }
    global String FieldMask {
        get;
        set;
    }
    global String FieldName {
        get;
        set;
    }
    global String FieldReadOnly {
        get;
        set;
    }
    global String FieldRequired {
        get;
        set;
    }
    global String FieldValue {
        get;
        set;
    }
    global String FieldVisibility {
        get;
        set;
    }
    global String HiddenField {
        get;
        set;
    }
    global QuickFormsFieldDef() {

    }
}
