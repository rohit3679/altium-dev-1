/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class QuikFormReLoadController {
    global Integer count {
        get;
        set;
    }
    global String outputHTML {
        get;
        set;
    }
    global String overridePage {
        get;
        set;
    }
    global QuikFormReLoadController() {

    }
    global void fetch() {

    }
}
