/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class QuikReadFinanialAccount {
    global List<FinServ__FinancialAccount__c> finAccnt {
        get;
        set;
    }
    global List<FinServ__FinancialAccountRole__c> inter {
        get;
        set;
    }
    global String inter_party {
        get;
        set;
    }
    global List<FinServ__FinancialAccountRole__c> rel {
        get;
        set;
    }
    global List<SkienceFinSln__Suitability__c> suit {
        get;
        set;
    }
    global QuikReadFinanialAccount() {

    }
    global void fetchAccntDetail(String ObjectId) {

    }
    global void fetchAccntDetailAccount(String Id) {

    }
    global void fetchAccntDetailCase(String Id) {

    }
    global void fetchAccntDetailContact(String Id) {

    }
    global void fetchAccntDetailFA(String AccountId) {

    }
    global List<FinServ__FinancialAccount__c> getAccountInfo(String AccountId) {
        return null;
    }
    global Map<Id,Account> getAccountInfo() {
        return null;
    }
    global Map<Id,Contact> getContactInfo(String AccountId) {
        return null;
    }
    global List<FinServ__FinancialAccountRole__c> getInterestPartyInfo(String AccountId) {
        return null;
    }
    global List<FinServ__FinancialAccountRole__c> getRelationInfo(String AccountId) {
        return null;
    }
    global List<SkienceFinSln__Suitability__c> getSuitabilityInfo(String AccountId) {
        return null;
    }
}
