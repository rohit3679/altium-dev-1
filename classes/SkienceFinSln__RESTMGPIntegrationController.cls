/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/GetHouseholdDetails/*')
global class RESTMGPIntegrationController {
    global RESTMGPIntegrationController() {

    }
    @HttpPost
    global static SkienceFinSln.RESTMGPIntegrationController.SkienceHousehold getHouseHoldDetail(String HouseholdId) {
        return null;
    }
global class FinancialAccount {
    global String AccountNumber;
    global String AccountRegistrationType;
    global List<SkienceFinSln.RESTMGPIntegrationController.FinancialAccountHolding> FinancialAccountHoldings;
    global String FinancialAccountRowId;
    global String ProductCompany;
    global String ProductName;
    global String ProductType;
    global FinancialAccount() {

    }
}
global class FinancialAccountHolding {
    global String CUSIP;
    global String HoldingId;
    global Decimal PositionValue;
    global String SecurityName;
    global String SecuritySymbol;
    global String SecurityType;
    global Decimal TotalQty;
    global Decimal UnitMultiplier;
    global Decimal UnitPrice;
    global Datetime ValuationDt;
    global FinancialAccountHolding() {

    }
}
global class HouseholdMember {
    global String BirthDate;
    global String FirstName;
    global String Gender;
    global String LastName;
    global String MemberId;
    global String MemberRole;
    global HouseholdMember() {

    }
}
global class SkienceHousehold {
    global Boolean AugmentWithPositions;
    global String errorMessage;
    global List<SkienceFinSln.RESTMGPIntegrationController.FinancialAccount> FinancialAccounts;
    global Boolean hasError;
    global String HouseholdId;
    global List<SkienceFinSln.RESTMGPIntegrationController.HouseholdMember> HouseholdMembers;
    global String HouseholdName;
    global String StateOfResidence;
    global SkienceHousehold() {

    }
}
}
