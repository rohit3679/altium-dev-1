/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RESTMGPIntegrationSFS implements SkienceFinSln.RESTMGPIntegration {
    global RESTMGPIntegrationSFS() {

    }
    global SkienceFinSln.RESTMGPIntegrationController.SkienceHousehold getHouseHoldDetails(String HouseholdId) {
        return null;
    }
    global static Map<String,Set<String>> getValidRelationShips() {
        return null;
    }
}
