/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SKCreateAOCustomProcessor implements SkienceFinSln.ICreateAOCustomProcessor {
    global SKCreateAOCustomProcessor() {

    }
    global void addCustomMaps(Map<String,String> mapCQuestionnaire, Map<String,String> mapAQuestionnaire, Map<String,String> mapSQuestionnaire) {

    }
}
