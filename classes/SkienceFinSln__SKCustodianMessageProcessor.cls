/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SKCustodianMessageProcessor implements SkienceFinSln.ICustodianMessageProcessor {
    global SKCustodianMessageProcessor() {

    }
    global String processCode(String responseCode) {
        return null;
    }
    global String processMessage(String responseMessage) {
        return null;
    }
}
