/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SKCustodianProcessor implements SkienceFinSln.ICustodianProcessor {
    global SKCustodianProcessor() {

    }
    global String getUpdateFinancialAccount(String clientEntityId) {
        return null;
    }
    global FinServ__FinancialAccount__c setShellDefault(FinServ__FinancialAccount__c financialAccount) {
        return null;
    }
    global SkienceFinSln.SubmitToCustodianResponse submitToCustodian(String financialAccountId, String method, Boolean isUpdate) {
        return null;
    }
}
