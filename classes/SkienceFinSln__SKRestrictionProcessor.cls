/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SKRestrictionProcessor implements SkienceFinSln.IRestrictionProcessor {
    global SKRestrictionProcessor() {

    }
    global SkienceFinSln__Financial_Account_Restriction__c ignoreRestriction(String restrictionId) {
        return null;
    }
    global String postRestrictions(String restrictionJSON) {
        return null;
    }
    global SkienceFinSln__Financial_Account_Restriction__c retryRestriction(String restrictionId) {
        return null;
    }
}
