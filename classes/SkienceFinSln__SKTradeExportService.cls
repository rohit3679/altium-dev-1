/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class SKTradeExportService {
    global SKTradeExportService() {

    }
    global virtual String invokeService(String tradeStr, String tradeExceptionstr, String tradeReviewLogStr, String optionStr) {
        return null;
    }
    global static SkienceFinSln.SKTradeExportService newInstance() {
        return null;
    }
    global virtual void parseResponse(String strResponse) {

    }
    global virtual void runExportService(String tradeStr, String tradeExceptionStr, String tradeReviewLogStr, String optionStr) {

    }
}
