/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ServicesHelper {
    global ServicesHelper() {

    }
    global static String decompressUsingDeflateStream(String strEncodedCompressedValue, String strOptionsJson) {
        return null;
    }
    global static String getAccountPositions(String strFinancialAccountRowId, String strOptionsJson) {
        return null;
    }
    global static String getAccountTransactions(String strFinancialAccountRowId, String strOptionsJson) {
        return null;
    }
    global static String getMonteCarloMeter(String strMGPInstId, String strSFAdvisorId, String strSFHouseHoldId, String strOptionsJson) {
        return null;
    }
    global static String invokeCustodianPrefillAPI(String strCustodian, String strAccountData, String strOptionsJson) {
        return null;
    }
    global static SkienceFinSln.ServicesHelper.ServiceResponse parseServiceResponse(String str) {
        return null;
    }
    global static String passThruToCustodian(String strCustodian, String strVerb, String strRequestId, String strPayload, String strOptionsJson) {
        return null;
    }
    global static String runOFACChecks(String strVendor, String strTestNames, String strAccountData, String strOptionsJson) {
        return null;
    }
    global static String runSuitabilityTests(String strVendor, String strTestConfiguration, String strAccountData, String strOptionsJson) {
        return null;
    }
    global static String submitAccountUpdateToCustodian(String strCustodian, String strAccountData, String strOptionsJson) {
        return null;
    }
    global static String submitNewAccountToCustodian(String strCustodian, String strAccountData, String strOptionsJson) {
        return null;
    }
    global static String testSubmitToCustodian(String strCustodian, String strVerb, String strAccountData, String strOptionsJson) {
        return null;
    }
    global static String transform(String strSymbolicName, String strVersion, String strInput, String strOptionsJson) {
        return null;
    }
    global static String transmitDocumentFolder(String strEndPointNames, String strDocumentFolderJson, String strOptionsJson) {
        return null;
    }
global class ServiceResponse {
    global String Code;
    global Object Data;
    global String Message;
    global String Status;
    global ServiceResponse() {

    }
    global Boolean isSuccess() {
        return null;
    }
    global void raiseException(String strContext) {

    }
}
global class ServicesException extends Exception {
}
}
