/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/SkncServicesREST/*')
global class ServicesREST {
    global ServicesREST() {

    }
    @HttpPost
    global static String activateConfig(String strCallbackId, String strChallengeText) {
        return null;
    }
}
