/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SharesAuditTrailLogs {
    global SharesAuditTrailLogs() {

    }
    global void LogShares(List<SObject> listShares, String operation, String processName) {

    }
}
