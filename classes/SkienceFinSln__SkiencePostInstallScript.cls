/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SkiencePostInstallScript implements System.InstallHandler {
    global SkiencePostInstallScript() {

    }
    global void createLaserAppSettings() {

    }
    global void createMappingObject() {

    }
    global void createNASUIWSSettings() {

    }
    global void createSkienceServiceSettings() {

    }
    global void createSkienceSettings() {

    }
    global void onInstall(System.InstallContext context) {

    }
    global void tradeExcpRecords() {

    }
}
