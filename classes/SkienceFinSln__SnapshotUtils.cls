/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SnapshotUtils {
    global SnapshotUtils() {

    }
    global static Set<Id> checkSnapshot(Map<Id,SObject> listOldRecord, Map<Id,SObject> listNewRecord) {
        return null;
    }
}
