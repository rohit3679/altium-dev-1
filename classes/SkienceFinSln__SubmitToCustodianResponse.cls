/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SubmitToCustodianResponse {
    global List<SkienceFinSln.AddedRestriction> addedRestrictions {
        get;
        set;
    }
    global String errorCode {
        get;
        set;
    }
    global String errorMessage {
        get;
        set;
    }
    global String faName {
        get;
        set;
    }
    global String faNumber {
        get;
        set;
    }
    global String line1 {
        get;
        set;
    }
    global String line2 {
        get;
        set;
    }
    global String line3 {
        get;
        set;
    }
    global String line4 {
        get;
        set;
    }
    global String line5 {
        get;
        set;
    }
    global String line6 {
        get;
        set;
    }
    global List<String> listAdditionalMessage {
        get;
        set;
    }
    global List<String> listError {
        get;
        set;
    }
    global List<String> listExcludeSection {
        get;
        set;
    }
    global Map<String,Map<String,List<String>>> mapCorrection {
        get;
        set;
    }
    global String status {
        get;
        set;
    }
    global Boolean success {
        get;
        set;
    }
    global String updateMessage {
        get;
        set;
    }
    global SubmitToCustodianResponse() {

    }
}
