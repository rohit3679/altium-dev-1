/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class UpdateTransactionField implements Database.Batchable<SObject>, Database.Stateful {
    global UpdateTransactionField(List<String> transactionList, Map<Id,Boolean> transIdAndStatusMap, Map<Id,String> transIdAndAnnotateAndEscalateMap, String status, String userId, String roleName, String annotation) {

    }
    global void execute(Database.BatchableContext BC, List<SkienceFinSln__Transactions__c> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
