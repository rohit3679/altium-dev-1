/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class ValidateQuikRecipientData {
    global String success;
    global ValidateQuikRecipientData() {

    }
    global static SkienceFinSln.ValidateQuikRecipientData newInstance() {
        return null;
    }
    global virtual String validateRecipientData(String recipientList, String objectId, String formIds, String msgData) {
        return null;
    }
}
