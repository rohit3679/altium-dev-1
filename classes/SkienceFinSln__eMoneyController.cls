/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class eMoneyController {
    global String eMoneyExtId {
        get;
        set;
    }
    global List<SObject> selectedContacts {
        get;
        set;
    }
    global String selectedContactsJson {
        get;
        set;
    }
    global String vfRetURLInSFX {
        get;
        set;
    }
    global eMoneyController(ApexPages.StandardController controller) {

    }
    global eMoneyController(ApexPages.StandardSetController controller) {

    }
}
