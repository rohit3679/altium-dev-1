/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class eMoneyImpl {
    global eMoneyImpl() {

    }
    global virtual Boolean deleteClient() {
        return null;
    }
    global virtual SkienceFinSln__SyncInfo__c getSyncInfo(Id sfid, String eMoneyId) {
        return null;
    }
    global virtual String getTranformXMLResponse(String strTranformXML, String strExceptionMsg) {
        return null;
    }
    global virtual String getTransformXML(List<SkienceFinSln.ObjectToJSONController.ObjectReturn> SFRecObjRet, String eMoneyTransformationName) {
        return null;
    }
    global virtual String massageMessage(String msg) {
        return null;
    }
    global static SkienceFinSln.eMoneyImpl newInstance() {
        return null;
    }
    global virtual String objectToJSONQueryName(Id sfid) {
        return null;
    }
    global virtual void processNotificationForAsync() {

    }
    global virtual System.HttpResponse sendRestRequest(String sBody, String operation, String httpMethod) {
        return null;
    }
    global virtual System.HttpResponse sendRestRequest(String sBody, String operation, String httpMethod, Integer successcode) {
        return null;
    }
    global virtual List<SObject> syncClientObject() {
        return null;
    }
    global virtual String transformationName(Id sfid) {
        return null;
    }
}
