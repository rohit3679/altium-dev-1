/*
*@author Athene Group LLC
*@date:- 08-01-2019
*@group Apex Class:- Batch
*@description:- Batch Class to update Account Team Members from the Primary member of the group
*/
global class UpdateAccountTeamMembersBatch implements Database.Batchable < sObject >, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String soql='Select id from Account where id In (Select AccountId From AccountContactRelation where FinServ__Primary__c = true and AccountId=\'001g0000022LClSAAW\')';
        return database.getquerylocator(soql);
    }
    global void execute(Database.BatchableContext BC,List<Account>scope) {
        Map<Id,List<AccountContactRelation>> parentchild =  new Map<Id, List<AccountContactRelation>>();
        Set<String> AccIdsSet = new Set<String>();
        List<Account> acid = new List<Account>();
        for(Account aid: scope){
            //  acid.[0]= aid[0].Id;
        }
        system.debug('Scope+---->'+scope);
        List<AccountContactRelation> accConList = [Select Contact.Accountid From AccountContactRelation
                                                   Where AccountId =:scope];
        System.debug('accConList------->'+accConList);
        for(AccountContactRelation accon : accConList){
            AccIdsSet.add(accon.Contact.Accountid);
        }
        //AccIdsSet.add(scope);
        system.debug('Child records'+AccIdsSet );
        List <AccountTeamMember> atmembers =[select id,CaseAccessLevel,AccountId,AccountAccessLevel,ContactAccessLevel,
                                             OpportunityAccessLevel,TeamMemberRole,UserId from AccountTeamMember 
                                             where AccountId In:AccIdsSet];
        system.debug('Account Team Members'+atmembers);
        
        
        /*   Map<Id,List<Account>>
Map<id, String> === store account id

List<AccountTeamMember> atmemlist= new List<AccountTeamMember>();
System.debug('0. Scope---->'+scope);
for(Account acid:scope){
AccId = acid.Id;
}

List<AccountContactRelation> accConList = [Select ID,AccountId,Contact.Accountid From AccountContactRelation
Where AccountId =:AccId];
system.debug('1. accConList---->'+accConList);
for(AccountContactRelation accon : accConList){
AccIdsSet.add(accon.Contact.Accountid);
}
system.debug('2.AccIdsSet-----> '+AccIdsSet);
List<AccountContactRelation> accConListPrimary = [Select AccountId,Contact.Accountid From AccountContactRelation 
Where AccountId =:AccId and FinServ__Primary__c = true];
system.debug('3.accConListPrimary-----> '+accConListPrimary);
for(AccountContactRelation accon : accConListPrimary){
AccIdsSetprimary.add(accon.Contact.Accountid);
}
system.debug('4.AccIdsSetprimary-----> '+AccIdsSetprimary);

//for()
List <AccountTeamMember> atmembers =[select id,CaseAccessLevel,AccountId,AccountAccessLevel,ContactAccessLevel,
OpportunityAccessLevel,TeamMemberRole,UserId from AccountTeamMember 
where AccountId In:AccIdsSetprimary];
system.debug('Account Team Members'+atmembers);

for(AccountTeamMember atm:atmembers){
atm.AccountId = atmembers[0].AccountId;
atm.CaseAccessLevel = atmembers[0].CaseAccessLevel;
atm.AccountAccessLevel = atmembers[0].AccountAccessLevel;
atm.ContactAccessLevel = atmembers[0].ContactAccessLevel;
atm.OpportunityAccessLevel = atmembers[0].OpportunityAccessLevel;
atm.TeamMemberRole = atmembers[0].TeamMemberRole;
atm.UserId = atmembers[0].UserId;
}*/
    }
    global void finish(Database.BatchableContext BC) {
    }
}