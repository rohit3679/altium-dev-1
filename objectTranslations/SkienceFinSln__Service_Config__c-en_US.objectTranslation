<?xml version="1.0" encoding="UTF-8"?>
<CustomObjectTranslation xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <help><!-- Authorized EVS user id that can be used for OFAC checks with EVS; do not change manually --></help>
        <label><!-- Authorized EVS User Id --></label>
        <name>SkienceFinSln__Authorized_EVS_User_Id__c</name>
    </fields>
    <fields>
        <help><!-- Authorized requestor id that can be used for IWS AO/TOA integration; do not change manually --></help>
        <label><!-- Authorized IWS Requestor Id --></label>
        <name>SkienceFinSln__Authorized_IWS_Requestor_Id__c</name>
    </fields>
    <fields>
        <help><!-- Authorized MGP inst id that can be used for MGP integration; do not change manually --></help>
        <label><!-- Authorized MGP Inst Id --></label>
        <name>SkienceFinSln__Authorized_MGP_Inst_Id__c</name>
    </fields>
    <fields>
        <help><!-- Authorized requestor id that can be used for NF straight thru processing and/or NASU integration; do not change manually --></help>
        <label><!-- Authorized NF Requestor Id --></label>
        <name>SkienceFinSln__Authorized_NF_Requestor_Id__c</name>
    </fields>
    <fields>
        <help><!-- Authorized super branch codes that can be used for NF Server to Server alert integration; do not change manually --></help>
        <label><!-- Authorized Super Branch Codes --></label>
        <name>SkienceFinSln__Authorized_NF_Super_Branch_Codes__c</name>
    </fields>
    <fields>
        <help><!-- Authorized user ids that can be used for Pershing straight thru processing; do not change manually --></help>
        <label><!-- Authorized Pershing User Ids --></label>
        <name>SkienceFinSln__Authorized_Pershing_User_Ids__c</name>
    </fields>
    <fields>
        <help><!-- Encoded value of the client certificate; export certificate in Base-64 encoded X.509 format, open it in text editor and copy/paste all content between the certificate begin and end lines --></help>
        <label><!-- Cert Encoded Value --></label>
        <name>SkienceFinSln__Client_Cert_Encoded_Value__c</name>
    </fields>
    <fields>
        <help><!-- Client certificate name; required to access Skience Services. You can create a certificate in Salesforce. Enter the name you assigned to the certificate here. --></help>
        <label><!-- Cert Name --></label>
        <name>SkienceFinSln__Client_Cert_Name__c</name>
    </fields>
    <fields>
        <help><!-- Password assigned by EVS; required if requesting EVS integration --></help>
        <label><!-- EVS Password --></label>
        <name>SkienceFinSln__EVS_Password__c</name>
    </fields>
    <fields>
        <help><!-- Status of last verification; do not change manually --></help>
        <label><!-- Last Verification Status --></label>
        <name>SkienceFinSln__Last_Verification_Status__c</name>
    </fields>
    <fields>
        <help><!-- Date/time when configuration was last verified; do not change manually --></help>
        <label><!-- Last Verified --></label>
        <name>SkienceFinSln__Last_Verified__c</name>
    </fields>
    <fields>
        <help><!-- Check to load and store account positions in Salesforce; uncheck to query on demand. Applies only if requesting data load. --></help>
        <label><!-- Load Positions --></label>
        <name>SkienceFinSln__Load_Positions__c</name>
    </fields>
    <fields>
        <help><!-- Check to load and store trade exception test results in Salesforce; uncheck to query on demand. Applies only if requesting data load and loading transactions.You must load trade exception test results to use trade blotter. --></help>
        <label><!-- Load Trade Exception Test Results --></label>
        <name>SkienceFinSln__Load_Trade_Exception_Test_Results__c</name>
    </fields>
    <fields>
        <help><!-- Check to load and store transactions in Salesforce; uncheck to query on demand. Applies only if requesting data load.You must load transactions to use trade blotter. --></help>
        <label><!-- Load Transactions --></label>
        <name>SkienceFinSln__Load_Transactions__c</name>
    </fields>
    <fields>
        <help><!-- Account lookup filter for NF Server to Server alert integration. This is used in addition to the account # for financial accounts not created through Skience. --></help>
        <label><!-- S2S Account Lookup Filter --></label>
        <name>SkienceFinSln__NF_S2S_Account_Lookup_Filter__c</name>
    </fields>
    <fields>
        <help><!-- Enter email ids that should receive notification emails for events such as configuration changes, certificate expiration etc. You can enter multiple email ids by separating them with commas. --></help>
        <label><!-- Notification Emails --></label>
        <name>SkienceFinSln__Notification_Email_Ids__c</name>
    </fields>
    <fields>
        <help><!-- Status of OAuth request; do not change manually --></help>
        <label><!-- OAuth Status --></label>
        <name>SkienceFinSln__OAuth_Status__c</name>
    </fields>
    <fields>
        <help><!-- Check if you want the server to re-generate client password; server will automatically assign initial password. Initial password does not need to be requested. --></help>
        <label><!-- Refresh Password? --></label>
        <name>SkienceFinSln__Refresh_Client_Password__c</name>
    </fields>
    <fields>
        <help><!-- Check to refresh existing OAuth or to refresh expired requests. OAuth request will be generated automatically if any of the requested integrations require it. You may check this option to generate an OAuth request even if it is otherwise not required. --></help>
        <label><!-- Refresh OAuth? --></label>
        <name>SkienceFinSln__Refresh_OAuth__c</name>
    </fields>
    <fields>
        <help><!-- Check to request data load; requires OAuth --></help>
        <label><!-- Request Data Load? --></label>
        <name>SkienceFinSln__Request_Data_Load__c</name>
    </fields>
    <fields>
        <help><!-- Check to request OFAC checks with EVS --></help>
        <label><!-- Request? --></label>
        <name>SkienceFinSln__Request_EVS_Integration__c</name>
    </fields>
    <fields>
        <help><!-- Check if requesting IWS AO/TOA integration --></help>
        <label><!-- Request? --></label>
        <name>SkienceFinSln__Request_IWS_Integration__c</name>
    </fields>
    <fields>
        <help><!-- Check to request MGP Integration; requires OAuth --></help>
        <label><!-- Request? --></label>
        <name>SkienceFinSln__Request_MGP_Integration__c</name>
    </fields>
    <fields>
        <label><!-- Request? --></label>
        <name>SkienceFinSln__Request_Morningstar_Integration__c</name>
    </fields>
    <fields>
        <help><!-- Check if requesting NASU integration --></help>
        <label><!-- Request NASU Integration? --></label>
        <name>SkienceFinSln__Request_NASU_Integration__c</name>
    </fields>
    <fields>
        <help><!-- Request NF paperwork integration; applies only if requesting NF straight thru processing. Requires OAuth. --></help>
        <label><!-- Request Paperwork Integration? --></label>
        <name>SkienceFinSln__Request_NF_Paperwork_Integration__c</name>
    </fields>
    <fields>
        <help><!-- Request NF server to server alerts integration; requires OAuth. --></help>
        <label><!-- Request Server To Server Alerts? --></label>
        <name>SkienceFinSln__Request_NF_S2S_Alerts__c</name>
    </fields>
    <fields>
        <help><!-- Check to request straight thru processing with NF --></help>
        <label><!-- Request NF Straight Thru Processing? --></label>
        <name>SkienceFinSln__Request_NF_STP__c</name>
    </fields>
    <fields>
        <help><!-- Check to request straight thru processing with Pershing --></help>
        <label><!-- Request? --></label>
        <name>SkienceFinSln__Request_Pershing_STP__c</name>
    </fields>
    <fields>
        <help><!-- User id assigned by EVS; required if requesting EVS integration. User id will be verified with EVS before it can be used. --></help>
        <label><!-- EVS User Id --></label>
        <name>SkienceFinSln__Requested_EVS_User_Id__c</name>
    </fields>
    <fields>
        <help><!-- Requestor id assigned by IWS; required if requesting AO/TOA integration with IWS. Requestor id will be verified with IWS before it can be used. --></help>
        <label><!-- IWS Requestor Id --></label>
        <name>SkienceFinSln__Requested_IWS_Requestor_Id__c</name>
    </fields>
    <fields>
        <help><!-- Inst id assigned by MoneyGuidePro; required if requesting MoneyGuidePro integration. Inst id will be verified with MoneyGuidPro before it can be used. --></help>
        <label><!-- MGP Inst Id --></label>
        <name>SkienceFinSln__Requested_MGP_Inst_Id__c</name>
    </fields>
    <fields>
        <help><!-- Requestor id assigned by NF; required if requesting NF straight through processing or NASU integration. Requestor id will be verified with NF before it can be used. --></help>
        <label><!-- NF Requestor Id --></label>
        <name>SkienceFinSln__Requested_NF_Requestor_Id__c</name>
    </fields>
    <fields>
        <help><!-- Super branch codes assigned by Pershing; required if requesting NF Server to Server alert integration. Super branch codes will be verified with NF before they can be used. Multiple super branch codes may be entered on separate lines. --></help>
        <label><!-- Super Branch Codes --></label>
        <name>SkienceFinSln__Requested_NF_Super_Branch_Codes__c</name>
    </fields>
    <fields>
        <help><!-- User ids assigned by Pershing; required if requesting straight thru processing with Pershing. User ids will be verified with Pershing before they can be used. Multiple user ids may be entered on separate lines. --></help>
        <label><!-- Pershing User Ids --></label>
        <name>SkienceFinSln__Requested_Pershing_User_Ids__c</name>
    </fields>
    <fields>
        <help><!-- URL filled in by the server; do not change manually --></help>
        <label><!-- Server URL --></label>
        <name>SkienceFinSln__Server_URL__c</name>
    </fields>
    <fields>
        <help><!-- Configuration status; do not change manually --></help>
        <label><!-- Status --></label>
        <name>SkienceFinSln__Status__c</name>
        <picklistValues>
            <masterLabel>Active</masterLabel>
            <translation><!-- Active --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Archived</masterLabel>
            <translation><!-- Archived --></translation>
        </picklistValues>
        <picklistValues>
            <masterLabel>Draft</masterLabel>
            <translation><!-- Draft --></translation>
        </picklistValues>
    </fields>
    <fields>
        <help><!-- Unique key; do not change manually --></help>
        <label><!-- Unique Key --></label>
        <name>SkienceFinSln__Unique_Key__c</name>
    </fields>
    <validationRules>
        <errorMessage><!-- EVS password required if requesting EVS integration --></errorMessage>
        <name>SkienceFinSln__Reqd_EVS_Password</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- EVS user id required if requesting EVS integration --></errorMessage>
        <name>SkienceFinSln__Reqd_Requested_EVS_User_Id</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- IWS requestor id required if requesting IWS AO/TOA integration --></errorMessage>
        <name>SkienceFinSln__Reqd_Requested_IWS_Requestor_Id</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- NF super branch codes required if requesting NF server to server alert integration --></errorMessage>
        <name>SkienceFinSln__Reqd_Requested_NF_Super_Branch_Codes</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- MGP inst id required if requesting MoneyGuidePro integration --></errorMessage>
        <name>SkienceFinSln__Required_Requested_MGP_Inst_Id</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- NF requestor id required if requesting NF straight thru processing or NASU integration --></errorMessage>
        <name>SkienceFinSln__Required_Requested_NF_Requestor_Id</name>
    </validationRules>
    <validationRules>
        <errorMessage><!-- Pershing user ids required if requesting Pershing straight thru processing --></errorMessage>
        <name>SkienceFinSln__Required_Requested_Pershing_User_Ids</name>
    </validationRules>
    <webLinks>
        <label><!-- Sync_with_Server --></label>
        <name>SkienceFinSln__Sync_with_Server</name>
    </webLinks>
    <webLinks>
        <label><!-- Verify --></label>
        <name>SkienceFinSln__Verify</name>
    </webLinks>
</CustomObjectTranslation>
