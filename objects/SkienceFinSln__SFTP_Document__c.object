<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Document within a SFTP folder</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableLicensing>false</enableLicensing>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>SkienceFinSln__Document_File_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>File name of the document</description>
        <externalId>false</externalId>
        <label>Document File Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Document_Id__c</fullName>
        <deprecated>false</deprecated>
        <description>Weak link to the document in the system</description>
        <externalId>false</externalId>
        <label>Document Id</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Document_Type__c</fullName>
        <deprecated>false</deprecated>
        <description>Document type for attachments manually uploaded</description>
        <externalId>false</externalId>
        <label>Document Type</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Endpoint_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>End point for this particular document. This could be populated when used for multiple end points</description>
        <externalId>false</externalId>
        <label>Endpoint Name</label>
        <length>120</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Form_Bundle_Id__c</fullName>
        <deprecated>false</deprecated>
        <description>If the document was sourced from a form bundle, this field stores the id of it</description>
        <externalId>false</externalId>
        <label>Form Bundle Id</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SkienceFinSln__SFTP_Folder__c</fullName>
        <deprecated>false</deprecated>
        <description>SFTP folder under which this document is added</description>
        <externalId>false</externalId>
        <label>SFTP Folder</label>
        <referenceTo>SkienceFinSln__SFTP_Folder__c</referenceTo>
        <relationshipName>SFTP_Documents</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Source__c</fullName>
        <deprecated>false</deprecated>
        <description>Source of the document</description>
        <externalId>false</externalId>
        <label>Source</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Ready</fullName>
                    <default>false</default>
                    <label>Ready</label>
                </value>
                <value>
                    <fullName>Not Ready</fullName>
                    <default>false</default>
                    <label>Not Ready</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Upload_End_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Upload end time, as reported by the Upload service</description>
        <externalId>false</externalId>
        <label>Upload End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Upload_Message__c</fullName>
        <deprecated>false</deprecated>
        <description>Message from the upload service. This would hold messages on success or failure of the service</description>
        <externalId>false</externalId>
        <label>Upload Message</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>SkienceFinSln__Upload_Start_Date__c</fullName>
        <deprecated>false</deprecated>
        <description>Upload service will update with the upload start date/time</description>
        <externalId>false</externalId>
        <label>Upload Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <label>Document</label>
    <nameField>
        <displayFormat>Doc-{00000000}</displayFormat>
        <label>Document Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Documents</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
