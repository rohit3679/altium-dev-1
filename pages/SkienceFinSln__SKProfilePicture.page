<apex:page standardController="Contact" extensions="SkienceFinSln.SKContactProfilePicController" sidebar="false" showHeader="false" standardStylesheets="false" >
    <apex:includeLightning />
    <apex:stylesheet value="{!URLFOR($Resource.SkienceFinSln__SLDS103, 'assets/styles/salesforce-lightning-design-system-vf.css')}" />
    <style type="text/css">
        img.circularMediumPhoto {
            border-radius: 50%;
            height: 5rem;
            width: 5rem;
            font-size: medium;
            color: rgb(217, 16, 209);
            border: .25px;
            background: rgba(255,255,255);
            max-width: 10rem;
            max-height: 10rem;
        }
        
        .slds-align--absolute-center {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-align-content: center;
            -ms-flex-line-pack: center;
            align-content: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            margin: auto;
        }
    </style>
    <script type="text/javascript">
        var maxStringSize = 6000000;    //Maximum String size is 6,000,000 characters
        var maxFileSize = 4350000;      //After Base64 Encoding, this is the max file size
        var chunkSize = 950000;         //Maximum Javascript Remoting message size is 1,000,000 characters
        var attachment;
        var attachmentName;
        var fileSize;
        var positionIndex;
        var doneUploading;
       
        //Method to prepare a file to be attached to the Account bound to the page by the standardController
        function uploadFile() {
            
            var file = document.getElementById('attachmentFile').files[0];
            if(file != undefined) {
                if(file.size <= maxFileSize) {
                    attachmentName = file.name;
                    var fileReader = new FileReader();
                    fileReader.onloadend = function(e) {
                        console.log('onloadend');
                        attachment = window.btoa(this.result);  //Base 64 encode the file before sending it
                        positionIndex=0;
                        fileSize = attachment.length;
                        console.log("Total Attachment Length: " + fileSize);
                        doneUploading = false;
                        if(fileSize < maxStringSize) {
                            uploadAttachment(null);
                        } else {
                            alert("Base 64 Encoded file is too large.  Maximum size is " + maxStringSize + " your file is " + fileSize + ".");
                        }
                    }
                    fileReader.onerror = function(e) {
                        alert("There was an error reading the file.  Please try again.");
                    }
                    fileReader.onabort = function(e) {
                        alert("There was an error reading the file.  Please try again.");
                    }                 
                    //For IE11
                    var reader = new FileReader();
                    var self =this;
                    if (!FileReader.prototype.readAsBinaryString) {
                        var binary = "";
                        reader.onloadend = function(e) {
                        var bytes = new Uint8Array(reader.result);
                        var length = bytes.byteLength;
                        for (var i = 0; i < length; i++) {
                           binary += String.fromCharCode(bytes[i]);
                        }
                        attachment = window.btoa(binary);  //Base 64 encode the file before sending it
                        positionIndex=0;
                        fileSize = attachment.length;
                        console.log("Total Attachment Length: " + fileSize);
                        doneUploading = false;
                        if(fileSize < maxStringSize) {
                            uploadAttachment(null);
                        } else {
                            alert("Base 64 Encoded file is too large.  Maximum size is " + maxStringSize + " your file is " + fileSize + ".");
                        }
                    }                     
                   reader.readAsArrayBuffer(file);
                 } else {
                       fileReader.readAsBinaryString(file);  //Read the body of the file
                 }
             
                } else {
                    alert("File must be under 4.3 MB in size.  Your file is too large.  Please try again.");
                }
            } else {
                alert("You must choose a file before trying to upload it");
            }
        }
        
        //Method to send a file to be attached to the Account bound to the page by the standardController
        //Sends parameters: Account Id, Attachment (body), Attachment Name, and the Id of the Attachment if it exists to the controller   
        function uploadAttachment(fileId) {
            var attachmentBody = "";
            if(fileSize <= positionIndex + chunkSize) {
                attachmentBody = attachment.substring(positionIndex);
                doneUploading = true;
            } else {
                attachmentBody = attachment.substring(positionIndex, positionIndex + chunkSize);
            }
            console.log("Uploading " + attachmentBody.length + " chars of " + fileSize);
            showSpinner();
            SkienceFinSln.SKContactProfilePicController.doUploadAttachment(
                '{!Contact.Id}', attachmentBody, attachmentName, fileId,
                function(result, event) {
                    console.log(result);
                    if(event.type === 'exception') {
                        console.log("exception");
                        console.log(event);
                    } else if(event.status) {
                        if(result.substring(0,3) == '00P') {
                            if(doneUploading == true) {
                                document.getElementById('imgProfile').src = 
                                    '/servlet/servlet.FileDownload?file=' + result;
                                hideSpinner();
                            } else {
                                positionIndex += chunkSize;
                                uploadAttachment(result);
                            }
                        }
                    } else {
                        console.log(event.message);
                    }
                },
                {buffer: true, escape: true, timeout: 120000}
            );
        }
        
        function showFileUpload(){
            document.getElementById('attachmentFile').click();
        }
        
        function showSpinner(){
            document.getElementById("spinner").className = 'slds-spinner_container';
        }
        
        function hideSpinner(){
            document.getElementById("spinner").className = 'slds-spinner_container slds-hide';
        }
    </script>
    
    <body> 
        <div class="slds">
            <div class="slds-form-element">
                <div class="slds-form-element__control">
                    <div class="slds-size--1-of-1 slds-align--absolute-center">
                        <span>
                            <img id="imgProfile" src="{!Contact.SkienceFinSln__Photo_URL__c}" class="circularMediumPhoto"  />
                        </span>
                    </div>
                </div>
                <div class="slds-form-element__control">
                    <div class="slds-size--1-of-1 slds-align--absolute-center slds-m-top--medium">
                        <button onclick="showFileUpload()" class="slds-button slds-button--neutral slds-button slds-button--neutral" type="button">
                            <input onchange="uploadFile()" class="slds-file-selector__input slds-assistive-text fileInput sdls-hide" accept="image/jpeg, image/pjpeg, image/png, image/x-png, image/gif" size="30" type="file" name="file" id="attachmentFile" tabindex="-1"/>
                            <span class="slds-icon_container slds-icon-utility-upload slds-button__icon slds-button__icon--left slds-button__icon forceIcon">
                                <svg class="slds-button__icon slds-button__icon--left">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/resource/SkienceFinSln__SLDS103/assets/icons/utility-sprite/svg/symbols.svg#upload"></use>
                                </svg>
                            </span>
                            Upload Image
                        </button>
                    </div>
                    <div class="slds-spinner_container slds-hide" id="spinner">
                        <div class="slds-spinner slds-spinner--brand slds-spinner--medium" role="status">
                            <span class="slds-assistive-text">Loading Profile Image</span>
                            <div class="slds-spinner__dot-a"></div>
                            <div class="slds-spinner__dot-b"></div>
                        </div>
                    </div>
                    <div class="slds-size--1-of-1 slds-align--absolute-center">
                        <span class="slds-file-selector__text slds-medium-show">Once uploaded, a preview of the image is shown for review.</span>
                    </div>
                </div>
            </div>
        </div>
    </body>
</apex:page>