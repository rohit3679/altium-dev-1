trigger AccountContactRelationTrigger on AccountContactRelation (After insert, After Update ) {
    Map<Id,AccountContactRelation> acrMap= new Map<Id,AccountContactRelation> ();
    List <Id> parentaccountId = new List<Id>();
    Id newAccId;
    Map<Id, List<AccountTeamMember>> rcmap = new Map<Id, List<AccountTeamMember>>();
    If(trigger.isInsert){
        for(AccountContactRelation acr: trigger.newMap.Values()){
            if(acr.FinServ__Primary__c==True){
                system.debug('True-------------------->');
                acrMAp.put(acr.Id,acr);
                // AccountContactRelationTriggerHandler.handleAccountTeam(acrMap);
            }
            else{
                parentaccountId.add(acr.AccountId);
                system.debug('***********parentaccountid' + parentaccountId);
                
            }
            List<AccountTeamMember> newTeamMemberList=[Select id, CaseAccessLevel,AccountId,AccountAccessLevel,ContactAccessLevel,
                                                       OpportunityAccessLevel,TeamMemberRole,UserId from AccountTeamMember 
                                                       where AccountId IN:parentaccountId ];
            if(newTeamMemberList.size()>0){
                
                
                for(AccountTeamMember newTeamMembers :newTeamMemberList)
                {
                    If(rcmap.containsKey(newTeamMembers.AccountId)){
                        List<AccountTeamMember> atmem = rcmap.get(newTeamMembers.AccountId);
                        atmem.add(newTeamMembers);
                        rcmap.put(newTeamMembers.AccountId, atmem);
                        system.debug('rcmap---if--->'+rcmap);
                        
                    } 
                    else{
                        rcmap.put(newTeamMembers.AccountId, new List<AccountTeamMember>{newTeamMembers});
                        system.debug('rcmap-----else-->'+rcmap);
                    }  
                    system.debug('rcmap--->'+rcmap);
                }
                
                //-----------------------------insert team members----------------------------
                map<ID,ID> parentAndChildAccIdMap = new Map<ID, ID>();
                Map<Id,List<Id>> acrIds= new Map<Id,List<Id>>();
                for(AccountContactRelation acr1: [Select id,Contact.Accountid,FinServ__Primary__c,AccountId From AccountContactRelation
                                                  Where AccountId In:parentaccountId])
                {
                    if(acr1.FinServ__Primary__c==True){
                        parentAndChildAccIdMap.put(acr1.AccountId, acr1.Contact.Accountid);
                        system.debug('parentAndChildAccIdMap---->'+parentAndChildAccIdMap);
                    }
                    If(acrIds.containsKey(acr1.AccountId)){
                        if(acr1.FinServ__Primary__c!= True){
                            List<ID> acrmem = acrIds.get(acr1.AccountId);
                            acrmem.add(acr1.Contact.Accountid);
                            acrIds.put(acr1.AccountId, acrmem);
                        } 
                        system.debug('acrIds--->'+acrIds);
                    }
                    else{
                        List<Id> accids = new List<Id>();
                        accids.add(acr1.AccountId);
                        if(acr1.FinServ__Primary__c!= True)
                            accids.add(acr1.Contact.Accountid);
                        acrIds.put(acr1.AccountId,accids);
                    }
                    system.debug('acr1------->'+acr1);
                }
                
                List<AccountTeamMember> atmList = new List<AccountTeamMember>();
                system.debug('acrIds=-- values()--->'+acrIds.values());
                //system.debug('acrIds.get(accid)---->'+acrIds.get(accid));
                for(Id accid: acrIds.Keyset()){
                    for(Id aid:acrIds.get(accid)){
                        for(AccountTeamMember ateamm: rcmap.get(accid) ){
                            
                            AccountTeamMember atm = new AccountTeamMember();
                            atm.AccountId = aid;
                            atm.CaseAccessLevel = ateamm.CaseAccessLevel;
                            atm.AccountAccessLevel = ateamm.AccountAccessLevel;
                            atm.ContactAccessLevel = ateamm.ContactAccessLevel;
                            atm.OpportunityAccessLevel = ateamm.OpportunityAccessLevel;
                            atm.TeamMemberRole = ateamm.TeamMemberRole;
                            atm.UserId = ateamm.UserId;
                            atmList.add(atm);
                        }
                    }
                }
                insert atmList;
                
                //----------------------------Insert Team Members----------------------------              
            } 
        }
        
        
        AccountContactRelationTriggerHandler.handleAccountTeam(acrMap);
    }
    
    
    If(trigger.isUpdate){
        for(AccountContactRelation acr: trigger.oldMap.Values()){
            if((trigger.newMap.get(acr.id).FinServ__Primary__c!= acr.FinServ__Primary__c) 
               && (trigger.newMap.get(acr.id).FinServ__Primary__c==TRUE)){
                   acrMAp.put(acr.Id,acr);
               }
        }
        AccountContactRelationTriggerHandler.handleAccountTeam(acrMap);
    }
}