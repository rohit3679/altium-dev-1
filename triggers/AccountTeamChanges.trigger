trigger AccountTeamChanges on Account(after insert, after update) 
{
   //list to hold new account team members
     List<AccountTeamMember> acctMembers = new List<AccountTeamMember>();
     
     //list to hold new account sharing rules
     List<AccountShare> acctSharingRules = new List<AccountShare>();
     
     //misc
     Set<String> rmMemberAccts = new Set<String>();
     Map<ID, List<ID>> AccountTeamMap = new Map<ID, List<ID>>(); 
    
    List<Account> acc = new list<Account>();
    
     //iterate through records to find update processor values
     for(Account a : Trigger.new)
     {
        
        if(Trigger.isInsert)
        {
            //new Account - Client Advisor
            //if(a.Client_Advisor__c != null){
            
            AccountTeamMember ca = new AccountTeamMember();
            ca.AccountId = a.Id;
            ca.TeamMemberRole = 'Client Advisor';
            //ca.UserId = a.Client_Advisor__c;
            acctMembers.add(ca);
            
            AccountShare caSharingRule = new AccountShare();
            caSharingRule.AccountId = a.Id;
            caSharingRule.OpportunityAccessLevel = 'Read';
            caSharingRule.CaseAccessLevel = 'Read';
            caSharingRule.AccountAccessLevel = 'Edit';
            //caSharingRule.UserOrGroupId = a.Client_Advisor__c;
            acctSharingRules.add(caSharingRule);
      //      }
        }
     }
}