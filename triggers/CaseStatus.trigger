/**
@Author : Rohit   
@Name : CaseStatus    
@Created Date : 7/12/2018

@Description :: This Trigger is used to Create two new records whenever Case Status ="Closed", for "New Account Opening" recordtype.
				The two new records will be of recordType "Administrative Platform Set Up" & "Trading Platform Set Up".
				The records will be assign to "Portfolio Operations" Queue.

@copyright(c) 2019-2020, The Athene Group.
@version : 1.0       
**/

trigger CaseStatus on Case (after update, after insert) {
    If(trigger.isUpdate){
        CaseStatusHandler.InsertTwoNewCase();
    }
    If(trigger.isInsert){
        CaseStatusHandler.InsertTwoNewCase();
    }
}