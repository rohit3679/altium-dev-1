trigger LineOfBusniessTrigger on Line_of_business__c (before insert,before update,before delete,after insert,after update,after delete,after Undelete) {
    LineOfBusniessTriggerhandler handler = new LineOfBusniessTriggerhandler();    
    if( Trigger.isInsert || Trigger.isUndelete)
    {
        if(Trigger.isAfter)
        {
            handler.OnAfterInsert(trigger.New);
        }
    }
    else if ( Trigger.isUpdate )
    {
        if(Trigger.isAfter)
        {
            handler.OnAfterUpdate(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    }
    else if ( Trigger.isDelete )
    {
        if(Trigger.isAfter)
        {
            handler.OnBeforeDelete(trigger.New ,trigger.Old,Trigger.NewMap,Trigger.OldMap);
        }
    }
}