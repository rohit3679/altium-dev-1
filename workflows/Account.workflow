<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Name_update</fullName>
        <field>SkienceFinSln__First_5_Name__c</field>
        <formula>if(CONTAINS(LEFT(Name, 5), &quot;&apos;&quot;),
	SUBSTITUTE(LEFT(Name, 5), &quot;&apos;&quot;, &quot;\&quot;&quot;) ,
LEFT(Name, 5)
)</formula>
        <name>Name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_EstablishmentDate_Year</fullName>
        <field>SkienceFinSln__Year_Establishment_Date__c</field>
        <formula>TEXT(YEAR(SkienceFinSln__Establishment_Date__c) )</formula>
        <name>Update EstablishmentDate Year</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>90 Review Date Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Review_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Client_Review_Approaching</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Review_Date__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Account EstablishmentDate Update</fullName>
        <actions>
            <name>SkienceFinSln__Update_EstablishmentDate_Year</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is used to update Extablishmentdate year</description>
        <formula>OR( AND(ISNEW() ,NOT(ISNULL(SkienceFinSln__Establishment_Date__c))), AND(ISCHANGED(SkienceFinSln__Establishment_Date__c), NOT(ISNULL(SkienceFinSln__Establishment_Date__c)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Account Name First5 Characters</fullName>
        <actions>
            <name>SkienceFinSln__Name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is used to update Name first 5 characters</description>
        <formula>OR( 	AND(ISNEW() ,NOT(ISNULL(Name))), 	AND(ISCHANGED(Name), NOT(ISNULL(Name)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Client_Review_Approaching</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Review_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Client Review Approaching</subject>
    </tasks>
</Workflow>
