<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Birth_Year_update</fullName>
        <field>SkienceFinSln__Year_BirthDate__c</field>
        <formula>TEXT(YEAR(Birthdate) )</formula>
        <name>Birth Year update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__TaxId_update</fullName>
        <field>SkienceFinSln__Last_4_SSN__c</field>
        <formula>RIGHT(FinServ__TaxId__c, 4)</formula>
        <name>TaxId update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Last_Name</fullName>
        <field>SkienceFinSln__First_5_LastName__c</field>
        <formula>if(CONTAINS(LEFT(LastName, 5), &quot;&apos;&quot;),
	SUBSTITUTE(LEFT(LastName, 5), &quot;&apos;&quot;, &quot;\&quot;&quot;) ,
LEFT(LastName, 5)
)</formula>
        <name>Update Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__Contact Birth Year Update</fullName>
        <actions>
            <name>SkienceFinSln__Birth_Year_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is used to update birth year</description>
        <formula>OR( 	AND(ISNEW() ,NOT(ISNULL(Birthdate))), 	AND(ISCHANGED(Birthdate), NOT(ISNULL(Birthdate)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Contact TaxId Last 4 digits</fullName>
        <actions>
            <name>SkienceFinSln__TaxId_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is used to update TaxId last 4 digits</description>
        <formula>OR( 	AND(ISNEW() ,NOT(ISNULL(FinServ__TaxId__c))), 	AND(ISCHANGED(FinServ__TaxId__c), NOT(ISNULL(FinServ__TaxId__c)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Last Name</fullName>
        <actions>
            <name>SkienceFinSln__Update_Last_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This is to update LastName first 5 characters</description>
        <formula>OR( 	AND(ISNEW() ,NOT(ISNULL(LastName))), 	AND(ISCHANGED(LastName), NOT(ISNULL(LastName)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
