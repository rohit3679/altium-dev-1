<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Account_Number_Update</fullName>
        <field>SkienceFinSln__Last_5_Account_Number__c</field>
        <formula>RIGHT(FinServ__FinancialAccountNumber__c, 5)</formula>
        <name>Account Number Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Clone_field</fullName>
        <field>SkienceFinSln__Clone__c</field>
        <literalValue>1</literalValue>
        <name>Update Clone field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Closed_Date</fullName>
        <field>FinServ__CloseDate__c</field>
        <name>Update Closed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Date_Closed</fullName>
        <field>FinServ__CloseDate__c</field>
        <formula>Today()</formula>
        <name>Update Date Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Primary_Owner_Id</fullName>
        <field>SkienceFinSln__Primary_Owner_Id__c</field>
        <formula>If( FinServ__PrimaryOwner__r.RecordType.DeveloperName = &quot;IndustriesIndividual&quot;, CASESAFEID(FinServ__PrimaryOwner__r.FinServ__PrimaryContact__c) , If( FinServ__PrimaryOwner__r.RecordType.DeveloperName = &quot;IndustriesBusiness&quot;, CASESAFEID(FinServ__PrimaryOwner__c) , null))</formula>
        <name>Update Primary Owner Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__FA Last 5 Account Number</fullName>
        <actions>
            <name>SkienceFinSln__Account_Number_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is used to update Account Number Last 5 digits</description>
        <formula>OR( 	AND(ISNEW() ,NOT(ISNULL(FinServ__FinancialAccountNumber__c))), 	AND(ISCHANGED(FinServ__FinancialAccountNumber__c), NOT(ISNULL(FinServ__FinancialAccountNumber__c)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Set Clone to True on FA</fullName>
        <actions>
            <name>SkienceFinSln__Update_Clone_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCLONE()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Closed Date When Status Changed from Closed</fullName>
        <actions>
            <name>SkienceFinSln__Update_Closed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow is used to make Closed Date as null when status changed from closed to something else.</description>
        <formula>AND(ISCHANGED(FinServ__Status__c),  	NOT(ISPICKVAL(FinServ__Status__c, &quot;Closed&quot;)), 	PRIORVALUE(FinServ__Status__c) = &quot;Closed&quot;  	)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Closed Date When Status is Closed</fullName>
        <actions>
            <name>SkienceFinSln__Update_Date_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow will update Date Closed field when FA status is closed</description>
        <formula>AND(ISCHANGED(FinServ__Status__c),  ISPICKVAL(FinServ__Status__c, &quot;Closed&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Primary Owner Id</fullName>
        <actions>
            <name>SkienceFinSln__Update_Primary_Owner_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is used to update Primary owner Id</description>
        <formula>OR( 	AND(ISNEW() ,NOT(ISNULL(FinServ__PrimaryOwner__c))), 	AND(ISCHANGED(FinServ__PrimaryOwner__c), NOT(ISNULL(FinServ__PrimaryOwner__c)))   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
