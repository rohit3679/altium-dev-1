<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_BD_Head_of_Lead_ready_for_Initial_assessment</fullName>
        <ccEmails>Rami.hamdan@silverlinecrm.com</ccEmails>
        <description>Alert BD Head of Lead ready for Initial assessment</description>
        <protected>false</protected>
        <recipients>
            <recipient>Business_Development_Division_Head</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Ready_for_Initial_Review</template>
    </alerts>
    <alerts>
        <fullName>Alert_BD_Head_of_Lead_ready_for_assessment</fullName>
        <ccEmails>Rami.hamdan@silverlinecrm.com</ccEmails>
        <description>Alert BD Head of Lead ready for assessment</description>
        <protected>false</protected>
        <recipients>
            <recipient>Business_Development_Division_Head</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Ready_for_Initial_Review</template>
    </alerts>
    <alerts>
        <fullName>Lead_Rejected</fullName>
        <description>Lead Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Lead_approved</fullName>
        <description>Lead approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_approved</template>
    </alerts>
    <alerts>
        <fullName>Notify_Anthony</fullName>
        <description>Notify Anthony</description>
        <protected>false</protected>
        <recipients>
            <recipient>adestefano@altiumwealth.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Ready_for_Final_Review</template>
    </alerts>
    <alerts>
        <fullName>Notify_to_the_approver</fullName>
        <description>Notify to the approver</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_review</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assign_to_Assess_Approval</fullName>
        <field>OwnerId</field>
        <lookupValue>Lead_Assess_Approval</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to Assess Approval</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CFQ_Received</fullName>
        <field>CFQ_Recieved__c</field>
        <literalValue>Completed</literalValue>
        <name>CFQ Received</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assessment_Approved</fullName>
        <field>Lead_Assessment_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Lead Assessment Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Assessment_Denied</fullName>
        <field>Lead_Assessment_Status__c</field>
        <literalValue>Denied</literalValue>
        <name>Lead Assessment Denied</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Lost</fullName>
        <field>Status</field>
        <literalValue>Closed Lost</literalValue>
        <name>Lead Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Type</fullName>
        <field>Opportunity_Type__c</field>
        <name>Opportunity Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval</fullName>
        <field>Lead_Assessment_Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Submitted for Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Approval_Status_to_Approved</fullName>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>Update Lead Approval Status	to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Approval_Status_to_Converted</fullName>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Converted&quot;</formula>
        <name>Update Lead Approval Status	to Converted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Approval_Status_to_Pending</fullName>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Pending&quot;</formula>
        <name>Update Lead Approval Status	to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Approval_Status_to_Rejected</fullName>
        <field>ApprovalStatus__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>Update Lead Approval Status	to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_to_Assess</fullName>
        <field>Status</field>
        <literalValue>Assess</literalValue>
        <name>Update Lead Status to Assess</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_status_to_Full_Discovery</fullName>
        <field>Status</field>
        <literalValue>Full discovery</literalValue>
        <name>Update Lead status to Full Discovery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_status_to_Qualified</fullName>
        <field>Status</field>
        <literalValue>Qualified</literalValue>
        <name>Update Lead status to Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Lead status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status_Date_with_Activity_Date</fullName>
        <field>Suspect_Date__c</field>
        <formula>LastActivityDate</formula>
        <name>Update Status Date with Activity Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_LastApproval_Assess</fullName>
        <field>LastApproval__c</field>
        <formula>&quot;Assess&quot;</formula>
        <name>update LastApproval - Assess</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_LastApproval_to_Initial_Discovery</fullName>
        <field>LastApproval__c</field>
        <formula>&quot;Initial Discovery&quot;</formula>
        <name>update LastApproval to Initial Discovery</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <tasks>
        <fullName>New_Lead_record_has_been_marked_as_ready_for_Assessment</fullName>
        <assignedTo>athene@altium.tag</assignedTo>
        <assignedToType>user</assignedToType>
        <description>New Lead record  has been marked as ready for Assessment.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>New Lead record  has been marked as ready for Assessment.</subject>
    </tasks>
    <tasks>
        <fullName>Schedule_Assess_Meeting</fullName>
        <assignedTo>Business_Development_Rep</assignedTo>
        <assignedToType>role</assignedToType>
        <description>The lead assessment has been approved. Schedule Assess meeting with prospect.</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Schedule Assess Meeting</subject>
    </tasks>
    <tasks>
        <fullName>Submit_Opportunity_Assessment_Form</fullName>
        <assignedTo>Business_Development_Rep</assignedTo>
        <assignedToType>role</assignedToType>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CFQ_Received_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Submit Opportunity Assessment Form</subject>
    </tasks>
</Workflow>
