<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Name</fullName>
        <field>Name</field>
        <formula>IF(Client__r.IsPersonAccount, 
Client__r.LastName &amp; &quot;, &quot; &amp; Client__r.FirstName, 
Client__r.Name) &amp;&quot; - &quot;&amp; RecordType.Name &amp;&quot; - &quot;&amp; TEXT(Status__c)</formula>
        <name>Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
