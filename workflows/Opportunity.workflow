<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Alert_CIO_of_Oppty_submission_for_approval</fullName>
        <ccEmails>shailaja.ginka@theathenegroup.com</ccEmails>
        <description>Alert CIO of Oppty submission for approval</description>
        <protected>false</protected>
        <recipients>
            <recipient>athene@altium.tag</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approval_review</template>
    </alerts>
    <alerts>
        <fullName>FP_Notification_to_approver</fullName>
        <description>FP - Notification to approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_Approval_review</template>
    </alerts>
    <alerts>
        <fullName>Insurance_CW_Notification_to_approver</fullName>
        <description>Insurance CW - Notification to approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Notification_to_approver_of_post_funding_compliance_check</template>
    </alerts>
    <alerts>
        <fullName>Insurance_Notification_to_approver</fullName>
        <description>Insurance - Notification to approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Notification_to_approver_of_pre_funding_compliance_check</template>
    </alerts>
    <alerts>
        <fullName>Invstmnt_Notification_to_approver</fullName>
        <description>Invstmnt - Notification to approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Notification_to_approver_of_pre_funding_compliance_check</template>
    </alerts>
    <alerts>
        <fullName>Invstmnt_PF_Notification_to_approver</fullName>
        <description>Invstmnt PF - Notification to approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Notification_to_approver_of_post_funding_compliance_check</template>
    </alerts>
    <alerts>
        <fullName>Oppty_approved</fullName>
        <description>Oppty approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Approved</template>
    </alerts>
    <alerts>
        <fullName>Oppty_rejected</fullName>
        <description>Oppty rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Rejected</template>
    </alerts>
    <alerts>
        <fullName>VP_Notification_to_approver</fullName>
        <description>VP - Notification to approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_Approval_review</template>
    </alerts>
    <alerts>
        <fullName>Vigilance_Notification_to_approver</fullName>
        <description>Vigilance - Notification to approver</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Opportunity_Approval_review</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Amount_for_AUM_Oppty_Tiers</fullName>
        <field>Amount</field>
        <formula>IF(
AUM_Amount__c &lt;= 1000000, (AUM_Amount__c * 0.0125) * (1 - AUM_Discount__c), 

IF(AND( AUM_Amount__c &gt; 1000000 , AUM_Amount__c &lt;= 5000000 )
, (1000000* 0.0125 + ((AUM_Amount__c - 1000000) * 0.0075)) * (1 - AUM_Discount__c)

, IF( AND( AUM_Amount__c &gt;5000000 , AUM_Amount__c &lt;= 10000000), 
((1000000* 0.0125) + (4000000 * 0.0075) + ((AUM_Amount__c - 5000000) * 0.005) ) * (1 - AUM_Discount__c)

,
((1000000* 0.0125) + (4000000 * 0.0075) + (5000000 * 0.005) + ((AUM_Amount__c - 10000000) * 0.0025)) * (1 - AUM_Discount__c)
)
)
)</formula>
        <name>Update Amount for AUM Oppty Tiers</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_By_field</fullName>
        <field>Approved_By__c</field>
        <formula>LastModifiedBy.FirstName&amp;&quot; &quot;&amp;LastModifiedBy.LastName</formula>
        <name>Update -  Approved By field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_OpptyStage_to_Implement</fullName>
        <field>StageName</field>
        <literalValue>Implement</literalValue>
        <name>Update OpptyStage to Implement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_ApprovalStatus_to_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Oppty ApprovalStatus to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_ApprovalStatus_to_Pending1</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Update Oppty ApprovalStatus to Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_ApprovalStatus_to_Proposal</fullName>
        <field>LastApproval__c</field>
        <formula>&apos;Proposal&apos;</formula>
        <name>Update Oppty ApprovalStatus to Proposal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_ApprovalStatus_to_Rejected</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Oppty ApprovalStatus to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_LastApproval_to_Compliance</fullName>
        <field>LastApproval__c</field>
        <formula>&apos;Compliance&apos;</formula>
        <name>Update Oppty LastApproval to Compliance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_LastApproval_to_Fee_approve</fullName>
        <field>LastApproval__c</field>
        <formula>&quot;Fee approved&quot;</formula>
        <name>Update Oppty LastApproval to Fee approve</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_Stage_to_Approve_proposal</fullName>
        <field>StageName</field>
        <literalValue>Approve proposal</literalValue>
        <name>Update Oppty Stage to Approve proposal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_Stage_to_Execute_agreement</fullName>
        <field>StageName</field>
        <literalValue>Execute agreement</literalValue>
        <name>Update Oppty Stage to Execute agreement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Agreement</fullName>
        <field>StageName</field>
        <literalValue>Agreement</literalValue>
        <name>Update Oppty stage to Agreement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Approve_Model</fullName>
        <field>StageName</field>
        <literalValue>Approve model</literalValue>
        <name>Update Oppty stage to Approve Model</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Audit_Model</fullName>
        <field>StageName</field>
        <literalValue>Audit &amp; Model</literalValue>
        <name>Update Oppty stage to Audit &amp; Model</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update Oppty stage to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Fund</fullName>
        <field>StageName</field>
        <literalValue>Fund financial account</literalValue>
        <name>Update Oppty stage to Fund</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Fund_Financial_Acc</fullName>
        <field>StageName</field>
        <literalValue>Fund financial account</literalValue>
        <name>Update Oppty stage to Fund Financial Acc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Prepare</fullName>
        <field>StageName</field>
        <literalValue>Prepare</literalValue>
        <name>Update Oppty stage to Prepare</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Present</fullName>
        <field>StageName</field>
        <literalValue>Present</literalValue>
        <name>Update Oppty stage to Present</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Oppty_stage_to_Verify_Compliance</fullName>
        <field>StageName</field>
        <literalValue>Verify compliance</literalValue>
        <name>Update Oppty stage to Verify Compliance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Submitted_By_field</fullName>
        <field>Submitted_By__c</field>
        <formula>LastModifiedBy.FirstName&amp;&quot; &quot;&amp;LastModifiedBy.LastName</formula>
        <name>Update - Submitted By field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check if AUM or Ins Oppty is closed won</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Opportunity_Type__c</field>
            <operation>equals</operation>
            <value>AUM,Insurance</value>
        </criteriaItems>
        <description>If AUM or Insurance Oppty is updated to be closed won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate AUM Oppty Amount</fullName>
        <actions>
            <name>Update_Amount_for_AUM_Oppty_Tiers</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This WFR will Automatically calculate the sale amount of an AUM opportunity with a tiered fee based on the asset amount and discount</description>
        <formula>AND(  RecordType.DeveloperName = &quot;AUM&quot; ,  ISPICKVAL(AUM_Fee_Type__c , &quot;Tiered&quot;) ,  NOT(ISBLANK(AUM_Amount__c )) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update -  Approved By</fullName>
        <actions>
            <name>Update_Approved_By_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((TEXT(PRIORVALUE( Approval_Status__c )) &lt;&gt; TEXT( Approval_Status__c)), ISPICKVAL(Approval_Status__c, &apos;Approved&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update -  Submitted By</fullName>
        <actions>
            <name>Update_Submitted_By_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND((TEXT(PRIORVALUE( Approval_Status__c )) &lt;&gt; TEXT( Approval_Status__c)), ISPICKVAL(Approval_Status__c, &apos;Submitted for Approval&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
