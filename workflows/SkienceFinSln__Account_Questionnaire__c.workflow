<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_AQ_Collection_2</fullName>
        <description>Set Screen Collection to &apos;AQ Collection 2&apos;</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;AQ Collection 2&apos;</formula>
        <name>Set Screen Collection - AQ Collection 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Account_Status_Code</fullName>
        <field>SkienceFinSln__Citizen_Status_Code__c</field>
        <literalValue>U.S. citizen</literalValue>
        <name>Update Account Status Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__AQ Collection 2</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_AQ_Collection_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rules for setting screen collection to &quot;AQ Collection 2&quot;</description>
        <formula>ISBLANK(SkienceFinSln__Primary_Owner__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Set Account Status Code on AQ</fullName>
        <actions>
            <name>SkienceFinSln__Update_Account_Status_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>CONTAINS(SkienceFinSln__Account_Type__c , &quot;IRA&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
