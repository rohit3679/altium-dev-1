<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Approval_Status</fullName>
        <field>SkienceFinSln__Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Completion_Date</fullName>
        <field>SkienceFinSln__Review_completion_date__c</field>
        <formula>NOW()</formula>
        <name>Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Is_Submitted</fullName>
        <field>SkienceFinSln__Is_Submitted__c</field>
        <literalValue>0</literalValue>
        <name>Is Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Rejected_Approval_Status</fullName>
        <field>SkienceFinSln__Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Rejected_Recall_Review_Status</fullName>
        <field>SkienceFinSln__Status__c</field>
        <literalValue>In Progress</literalValue>
        <name>Rejected/Recall Review Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Review_Status</fullName>
        <field>SkienceFinSln__Status__c</field>
        <literalValue>Completed</literalValue>
        <name>Review Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
