<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Name</fullName>
        <field>Name</field>
        <formula>IF(NOT(ISBLANK(SkienceFinSln__service_feature_type_code__c )) || NOT(ISBLANK(SkienceFinSln__service_feature_sub_type_code__c )), SkienceFinSln__service_feature_type_code__c +&apos; &apos;+&apos;&amp;&apos;+&apos; &apos;+ SkienceFinSln__service_feature_sub_type_code__c, null)</formula>
        <name>Update Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__Set Name field</fullName>
        <actions>
            <name>SkienceFinSln__Update_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(NOT(ISBLANK(SkienceFinSln__service_feature_type_code__c)), SkienceFinSln__service_feature_type_code__c != null, NOT(ISBLANK(SkienceFinSln__service_feature_sub_type_code__c)), SkienceFinSln__service_feature_sub_type_code__c != null)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
