<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_CQ_Collection_2</fullName>
        <description>Set Screen Collection to &apos;CQ Collection 2&apos;</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;CQ Collection 2&apos;</formula>
        <name>Set Screen Collection - CQ Collection 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_CQ_Collection_3</fullName>
        <description>Set Screen Collection - CQ Collection 3</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;CQ Collection 3&apos;</formula>
        <name>Set Screen Collection - CQ Collection 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Account_Status_Code</fullName>
        <field>SkienceFinSln__Citizen_Status_Code__c</field>
        <literalValue>U.S. citizen</literalValue>
        <name>Update Account Status Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__CQ Collection 2</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_CQ_Collection_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rules for setting screen collection to &quot;CQ Collection 2&quot;</description>
        <formula>AND( ISBLANK(SkienceFinSln__Contact__c),  NOT(AND(SkienceFinSln__Role_Name__c = &apos;Beneficial Owner&apos;, OR(SkienceFinSln__Account_Type__c = &apos;TOD - Joint&apos;, SkienceFinSln__Account_Type__c = &apos;TOD - Individual&apos;, SkienceFinSln__Account_Type__c = &apos;TOD - Joint Tenants by Entirety&apos;))))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__CQ Collection 3</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_CQ_Collection_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rules for setting screen collection to &quot;CQ Collection 3&quot;</description>
        <formula>AND( ISBLANK(SkienceFinSln__Contact__c),  SkienceFinSln__Role_Name__c = &apos;Beneficial Owner&apos;, OR(SkienceFinSln__Account_Type__c = &apos;TOD - Joint&apos;, SkienceFinSln__Account_Type__c = &apos;TOD - Individual&apos;, SkienceFinSln__Account_Type__c = &apos;TOD - Joint Tenants by Entirety&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Set Account Status Code on CQ</fullName>
        <actions>
            <name>SkienceFinSln__Update_Account_Status_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>CONTAINS(SkienceFinSln__Account_Type__c , &quot;IRA&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
