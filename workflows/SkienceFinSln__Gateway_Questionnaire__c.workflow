<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_GQ_Collection_1</fullName>
        <description>Set Screen Collection to &apos;GQ Collection 1&apos;</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;GQ Collection 1&apos;</formula>
        <name>Set Screen Collection - GQ Collection 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_GQ_Collection_2</fullName>
        <description>Set Screen Collection to &apos;GQ Collection 2&apos;</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;GQ Collection 2&apos;</formula>
        <name>Set Screen Collection - GQ Collection 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__GQ Collection 1</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_GQ_Collection_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rules for setting screen collection to &quot;GQ Collection 1&quot;</description>
        <formula>AND( NOT(ISBLANK( SkienceFinSln__Contact__c )),  ISPICKVAL(SkienceFinSln__Is_Entity_Contact__c, &apos;Person&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__GQ Collection 2</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_GQ_Collection_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rules for setting screen collection to &quot;GQ Collection 2&quot;</description>
        <formula>AND(NOT(ISBLANK( SkienceFinSln__Account__c )),  ISPICKVAL( SkienceFinSln__Is_Entity_Contact__c , &apos;Entity&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
