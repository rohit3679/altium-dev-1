<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Clone_on_Page_Manager</fullName>
        <description>Set the Clone field on the Page Manager to True</description>
        <field>SkienceFinSln__Clone__c</field>
        <literalValue>1</literalValue>
        <name>Set Clone on Page Manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__Page Manager Clone</fullName>
        <actions>
            <name>SkienceFinSln__Set_Clone_on_Page_Manager</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Identifies and sets the clone flag in page manager</description>
        <formula>ISCLONE ()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
