<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Financial_Account_Name_Set_Update</fullName>
        <description>Checks the Financial Account Name Set checkbox</description>
        <field>SkienceFinSln__Financial_Account_Name_Set__c</field>
        <literalValue>1</literalValue>
        <name>Financial Account Name Set Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Clone_on_Questionnaire</fullName>
        <description>Set the Clone field on the Questionnaire to True</description>
        <field>SkienceFinSln__Clone__c</field>
        <literalValue>1</literalValue>
        <name>Set Clone on Questionnaire</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_Q_Collection_1</fullName>
        <description>Set Screen Collection to &apos;Q Collection 1&apos;</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;Q Collection 1&apos;</formula>
        <name>Set Screen Collection - Q Collection 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_Q_Collection_2</fullName>
        <description>Set Screen Collection to &apos;Q Collection 2&apos;</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;Q Collection 2&apos;</formula>
        <name>Set Screen Collection - Q Collection 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Update_Product_Level_Sub_Type</fullName>
        <field>SkienceFinSln__Product_Level_Sub_Type__c</field>
        <literalValue>A - Advisory</literalValue>
        <name>Update Product Level Sub-Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__Financial Account Name Set Rule</fullName>
        <actions>
            <name>SkienceFinSln__Financial_Account_Name_Set_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Checks the financial account name set checkbox when the financial account name is set</description>
        <formula>SkienceFinSln__Financial_Account_Name__c != null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Q Collection 1</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_Q_Collection_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rules for setting screen collection to &quot;Q Collection 1&quot;</description>
        <formula>AND(  TEXT(SkienceFinSln__Product_Type__c) != &apos;Fixed Annuity&apos;,  TEXT(SkienceFinSln__Product_Type__c) != &apos;Group Annuity&apos;,  TEXT(SkienceFinSln__Product_Type__c) != &apos;Immediate Annuity&apos;,  TEXT(SkienceFinSln__Product_Type__c) != &apos;Variable Annuity&apos;,  TEXT(SkienceFinSln__Product_Type__c) != &apos;Variable Life&apos;, TEXT(SkienceFinSln__Product_Type__c) != &apos;Mutual Fund&apos;, TEXT(SkienceFinSln__Product_Type__c) != &apos;Equity Indexed Annuity&apos;  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Q Collection 2</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_Q_Collection_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Rules for setting screen collection to &quot;Q Collection 2&quot;</description>
        <formula>OR( TEXT(SkienceFinSln__Product_Type__c) = &apos;Fixed Annuity&apos;, TEXT(SkienceFinSln__Product_Type__c) = &apos;Group Annuity&apos;, TEXT(SkienceFinSln__Product_Type__c) = &apos;Immediate Annuity&apos;, TEXT(SkienceFinSln__Product_Type__c) = &apos;Variable Annuity&apos;, TEXT(SkienceFinSln__Product_Type__c) = &apos;Variable Life&apos;, TEXT(SkienceFinSln__Product_Type__c) = &apos;Mutual Fund&apos;, TEXT(SkienceFinSln__Product_Type__c) = &apos;Equity Indexed Annuity&apos;  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Questionnaire Clone</fullName>
        <actions>
            <name>SkienceFinSln__Set_Clone_on_Questionnaire</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Identifies and sets the clone flag in questionnaire</description>
        <formula>ISCLONE()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Product Level Sub-Type</fullName>
        <actions>
            <name>SkienceFinSln__Update_Product_Level_Sub_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SkienceFinSln__Questionnaire__c.SkienceFinSln__Product_Level__c</field>
            <operation>equals</operation>
            <value>UNMGD ACCT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
