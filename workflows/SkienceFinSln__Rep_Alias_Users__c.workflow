<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__FU_RepAliasUserName</fullName>
        <field>Name</field>
        <formula>IF(
OR(ISNULL(SkienceFinSln__User__c),ISBLANK(SkienceFinSln__User__c)), 
SkienceFinSln__rep_alias__r.Name &amp; &apos;-Group-&apos; &amp; SkienceFinSln__Group_Name__c,
SkienceFinSln__rep_alias__r.Name &amp; &apos;-&apos; &amp;  SkienceFinSln__User__r.FirstName &amp; &apos; &apos;&amp; SkienceFinSln__User__r.LastName
)</formula>
        <name>FU_RepAliasUserName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__FU_RepAliasUserNameUnique</fullName>
        <field>SkienceFinSln__RepAliasUserName__c</field>
        <formula>Name</formula>
        <name>FU_RepAliasUserNameUnique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__Rep Alias Users Name Override</fullName>
        <actions>
            <name>SkienceFinSln__FU_RepAliasUserName</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SkienceFinSln__FU_RepAliasUserNameUnique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>SkienceFinSln__Rep_Alias_Users__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
