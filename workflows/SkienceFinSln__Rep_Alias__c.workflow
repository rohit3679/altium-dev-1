<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__FU_RepAliasname</fullName>
        <field>Name</field>
        <formula>SkienceFinSln__RepCodeType__c &amp; &apos;-&apos; &amp; SkienceFinSln__rep_code__c</formula>
        <name>FU_RepAliasname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__FU_RepAliasnameUnique</fullName>
        <field>SkienceFinSln__RepAliasName__c</field>
        <formula>Name</formula>
        <name>FU_RepAliasnameUnique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__Rep Alias Name Override</fullName>
        <actions>
            <name>SkienceFinSln__FU_RepAliasname</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SkienceFinSln__FU_RepAliasnameUnique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Rep Alias name is empty then the combination of Rep Alias type and Rep code will be save in this name field.</description>
        <formula>Name   = Id</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
