<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_StQ_Collection_1</fullName>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;StQ Collection 1&apos;</formula>
        <name>Set Screen Collection - StQ Collection 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_StQ_Collection_2</fullName>
        <description>Set the screen collection to StQ Collection 2</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;StQ Collection 2&apos;</formula>
        <name>Set Screen Collection - StQ Collection 2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_StQ_Collection_3</fullName>
        <description>Set screen collection to &apos;StQ Collection 3&apos;</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;StQ Collection 3&apos;</formula>
        <name>Set Screen Collection - StQ Collection 3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Screen_Collection_StQ_Collection_4</fullName>
        <description>Set screen collection to StQ Collection 4</description>
        <field>SkienceFinSln__Screen_Collection__c</field>
        <formula>&apos;StQ Collection 4&apos;</formula>
        <name>Set Screen Collection - StQ Collection 4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__StQ Collection 1</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_StQ_Collection_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( SkienceFinSln__Stakeholder__c != null, OR( TEXT(SkienceFinSln__Stakeholder_Type__c) = &apos;Beneficiary&apos;, TEXT(SkienceFinSln__Stakeholder_Type__c) = &apos;Interested Party&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__StQ Collection 2</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_StQ_Collection_2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( SkienceFinSln__Stakeholders_Contact__c != null, OR( TEXT(SkienceFinSln__Stakeholder_Type__c) = &apos;Beneficiary&apos;, TEXT(SkienceFinSln__Stakeholder_Type__c) = &apos;Interested Party&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__StQ Collection 3</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_StQ_Collection_3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( SkienceFinSln__Stakeholder__c != null, AND( TEXT(SkienceFinSln__Stakeholder_Type__c) != &apos;Beneficiary&apos;, TEXT(SkienceFinSln__Stakeholder_Type__c) != &apos;Interested Party&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__StQ Collection 4</fullName>
        <actions>
            <name>SkienceFinSln__Set_Screen_Collection_StQ_Collection_4</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set screen collection to StQ Collection 4</description>
        <formula>AND( SkienceFinSln__Stakeholders_Contact__c != null, AND( TEXT(SkienceFinSln__Stakeholder_Type__c) != &apos;Beneficiary&apos;, TEXT(SkienceFinSln__Stakeholder_Type__c) != &apos;Interested Party&apos; ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
