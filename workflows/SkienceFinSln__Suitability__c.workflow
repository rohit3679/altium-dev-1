<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Suitability_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>SkienceFinSln__Suitability_Draft_Approved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Suitability Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Suitability_Record_Type_to_Annuity</fullName>
        <field>RecordTypeId</field>
        <lookupValue>SkienceFinSln__Suitability_Annuity</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Suitability Record Type to Annuity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Suitability_Record_Type_to_DOL</fullName>
        <field>RecordTypeId</field>
        <lookupValue>SkienceFinSln__Suitability_DOL</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Suitability Record Type to DOL</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SkienceFinSln__Set_Suitability_Record_Type_to_Pershing</fullName>
        <field>RecordTypeId</field>
        <lookupValue>SkienceFinSln__Suitability_Pershing</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Suitability Record Type to Pershing</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SkienceFinSln__Update Suitability Record Type to Draft Approved</fullName>
        <actions>
            <name>SkienceFinSln__Set_Suitability_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK(SkienceFinSln__Suitability_Questionnaire__c), NOT(ISBLANK(SkienceFinSln__FSC_Financial_Account__c)), NOT(CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Account_Type__c), &quot;IRA&quot;)), OR(NOT(CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Product_Type__c), &quot;Annuity&quot;))), NOT(CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Product_Type__c), &quot;Variable Life&quot;)), NOT(CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Product_Company__c), &quot;Pershing&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Suitability Record Type to Suitability - DOL</fullName>
        <actions>
            <name>SkienceFinSln__Set_Suitability_Record_Type_to_DOL</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK(SkienceFinSln__Suitability_Questionnaire__c), NOT(ISBLANK(SkienceFinSln__FSC_Financial_Account__c)), CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Account_Type__c), &quot;IRA&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Suitability Record Type to Suitability Annuity</fullName>
        <actions>
            <name>SkienceFinSln__Set_Suitability_Record_Type_to_Annuity</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK(SkienceFinSln__Suitability_Questionnaire__c), NOT(ISBLANK(SkienceFinSln__FSC_Financial_Account__c)), OR(CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Product_Type__c), &quot;Annuity&quot;), CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Product_Type__c), &quot;Variable Life&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SkienceFinSln__Update Suitability Record Type to Suitability Pershing</fullName>
        <actions>
            <name>SkienceFinSln__Set_Suitability_Record_Type_to_Pershing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISBLANK(SkienceFinSln__Suitability_Questionnaire__c), NOT(ISBLANK(SkienceFinSln__FSC_Financial_Account__c)), CONTAINS(TEXT(SkienceFinSln__FSC_Financial_Account__r.SkienceFinSln__Product_Company__c), &quot;Pershing&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
