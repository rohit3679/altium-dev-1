<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Action_Mode</fullName>
        <field>ActionMode__c</field>
        <formula>IF(INCLUDES( ServiceOffering__r.ActionMode__c, &quot;Clarity&quot;), &quot;Clarity&quot;, NULL)  + BR() + 
IF(INCLUDES( ServiceOffering__r.ActionMode__c, &quot;Creation&quot;), &quot;Creation&quot;, NULL)  + BR() + 
IF(INCLUDES( ServiceOffering__r.ActionMode__c, &quot;Action&quot;), &quot;Action&quot;, NULL) + BR() + 
IF(INCLUDES( ServiceOffering__r.ActionMode__c, &quot;Monitor&quot;), &quot;Monitor&quot;, NULL)</formula>
        <name>Update Action Mode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Estimated_hours</fullName>
        <field>EstimatedHours__c</field>
        <formula>ServiceOffering__r.EstimatedHours__c</formula>
        <name>Update Estimated hours</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_VS_Description</fullName>
        <field>Description__c</field>
        <formula>ServiceOffering__r.Description__c</formula>
        <name>Update VS Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Discipline</fullName>
        <field>Discipline__c</field>
        <formula>TEXT( ServiceOffering__r.Discipline__c )</formula>
        <name>update Discipline</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
